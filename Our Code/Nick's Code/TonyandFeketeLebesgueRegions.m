tic;
allFeketepnts=1200;
numpoints=1:allFeketepnts;
%x and y are vector form of meshgrid common to all shapes
spacing=100;
X=linspace(-1,1,spacing);
[xx,yy]=meshgrid(X);
x=xx(:);
y=yy(:);
%create star with hole
% define boundary
theta = linspace(0,2*pi,1000);
xbstar = ((0.9+0.1*cos(12*theta)).*cos(theta))';
ybstar = ((0.9+0.1*cos(12*theta)).*sin(theta))';
% select points inside the boundary
ind = inpolygon(xx(:),yy(:),xbstar,ybstar);
xstar = x(ind);
ystar = y(ind);
incirc = xstar.^2+ystar.^2 < .1^2;
xstar(incirc) = [];
ystar(incirc) = [];
%create square
ind=(x==-1)|(x==1)|...
    (y==-1)|(y==1);
xsquare=x(~ind);
ysquare=y(~ind);
xbsquare=x(ind);
ybsquare=y(ind);
%create circle
xbcircle=(cos(theta))';
ybcircle=(sin(theta))';
ind=inpolygon(x,y,xbcircle,ybcircle);
xcircle=x(ind);
ycircle=y(ind);
%create triangle
%vertices (-1,1),(-1,-1),(0.4,0.6)
%edge from (-1,1) to (0.4,0.6)
x1=linspace(-1,0.4,2*spacing);
x1(end)=[];
y1=(-2*x1+5)/7;
%edge from (0.4,0.6) to (-1,-1)
x2=linspace(0.4,-1,2*spacing);
x2(end)=[];
y2=(8*x2+1)/7;
%edge from (-1,-1) to (-1,1)
y3=linspace(-1,1,2*spacing);
y3(end)=[];
x3=-1*ones(1,2*spacing-1);
xbtriangle=[x1,x2,x3]';
ybtriangle=[y1,y2,y3]';
%xt and yt are denser meshgrids to get more points inside triangle
[xxt,yyt]=meshgrid(linspace(-1,0.4,floor(1.1*spacing)),linspace(-1,1,floor(1.1*spacing)));
xt=xxt(:);
yt=yyt(:);
ind=inpolygon(xt,yt,xbtriangle,ybtriangle);
xtriangle=xt(ind);
ytriangle=yt(ind);
ind=xtriangle==-1;
xtriangle(ind)=[];
ytriangle(ind)=[];
%create L-shape (square with bottom right cut out)
%edge 1 (-1,-1) to (-1,1)
x1=-1*ones(1,spacing-1);
y1=linspace(-1,1,spacing);
y1(end)=[];
%edge 2 from (-1,1) to (1,1)
x2=linspace(-1,1,spacing);
x2(end)=[];
y2=ones(1,spacing-1);
%edge from (1,1) to (1,0)
x3=ones(1,ceil(spacing/2)-1);
y3=linspace(1,0,ceil(spacing/2));
y3(end)=[];
%edge from (1,0) to (0,0)
x4=linspace(1,0,ceil(spacing/2));
x4(end)=[];
y4=zeros(1,ceil(spacing/2)-1);
%edge from (0,0) to (0,-1)
x5=zeros(1,ceil(spacing/2)-1);
y5=linspace(0,-1,ceil(spacing/2));
y5(end)=[];
%edge from (0,-1) to (-1,-1)
x6=linspace(0,-1,ceil(spacing/2));
x6(end)=[];
y6=-1*ones(1,ceil(spacing/2)-1);
xbL=[x1,x2,x3,x4,x5,x6]';
ybL=[y1,y2,y3,y4,y5,y6]';
ind=inpolygon(x,y,[-1,-1,1,1,0,0],[-1,1,1,0,0,-1]);
xL=x(ind);
yL=y(ind);
ind=(xL==-1)|(xL==1)|(yL==-1)|(yL==1);
xL(ind)=[];
yL(ind)=[];
%regions
shapes={...
    {'star with hole',xbstar,ybstar,xstar,ystar},...
    {'square',xbsquare,ybsquare,xsquare,ysquare},...
    {'circle',xbcircle,ybcircle,xcircle,ycircle},...
    {'scalene triangle',xbtriangle,ybtriangle,xtriangle,ytriangle},...
    {'L bottom right missing',xbL,ybL,xL,yL}...
    };
%Lebesgue Constants
LCsFeketewithbounds=zeros(1,allFeketepnts,length(shapes));
LCsFeketeLSwithbounds=zeros(1,allFeketepnts,length(shapes));
LCsTonyLSwithbounds=zeros(1,allFeketepnts,length(shapes));
LCsFekete=zeros(1,allFeketepnts,length(shapes));
LCsFeketeLS=zeros(1,allFeketepnts,length(shapes));
LCsTonyLS=zeros(1,allFeketepnts,length(shapes));
%tonyLS is indices of Tony's approximation greedy points in x and y vectors
%needs to have one extraneous point because of the way for loop is
%written
tonyLSwithbounds=zeros(allFeketepnts+1,1,length(shapes));
tonyLS=zeros(allFeketepnts+1,1,length(shapes));
%random error to be introduced to tony points and tonyLS points
errlvl=10^-4;
errorsTonyLSfuncs=(2*rand(allFeketepnts,1,length(shapes))-1)*errlvl;
for i=1:length(shapes)
    xb=shapes{i}{2};
    yb=shapes{i}{3};
    x=shapes{i}{4};
    y=shapes{i}{5};
    %each row of A is a fine point, each column to be a basis function
    A=zeros(length([x;xb]),allFeketepnts);
    %c is A's column counter
    c=1;
    j=0;%total degree
    k=0;%degree on y vector
    while c<=allFeketepnts
        %create new column of A
        if k==j
            if j==0
                A(:,c)=ones(length([x;xb]),1);
            else
                A(:,c)=A(:,c-(j+1)).*[y;yb];
            end
            k=0;
            j=j+1;
        else
            A(:,c)=A(:,c-j).*[x;xb];
            k=k+1;
        end
        %orthonormalize new column of A
        for l=1:c-1
            dot=(A(:,c)'*A(:,l));
            A(:,c)=A(:,c)-dot*A(:,l);
        end
        normAc=norm(A(:,c));
        A(:,c)=A(:,c)/normAc;
        if c>=20
            %Calculate c Fekete points including boundary
            b=rand(c,1);
            dummywithbounds=(A(:,1:c))'\b;
            indwithbounds=dummywithbounds~=0;
            %Calculate c Fekete points excluding boundary
            dummy=(A(1:length(x),1:c))'\b;
            ind=dummy~=0;
            %Calculate Lagrange functions with bounds
            lagrangecoeffswithbounds=pinv(A(indwithbounds,1:c));
            lagrangeswithbounds=A(:,1:c)*lagrangecoeffswithbounds;
            %Calculate Lagrange functions without bounds
            lagrangecoeffs=pinv(A(ind,1:c));
            lagranges=A(1:length(x),1:c)*lagrangecoeffs;
            %Calculate pseuodoLagrange functions for Least Squares with bounds
            LSlagrangecoeffswithbounds=pinv(A(indwithbounds,1:floor(c/2)));
            LSlagrangeswithbounds=A(:,1:floor(c/2))*LSlagrangecoeffswithbounds;
            %Calculate pseuodoLagrange functions for Least Squares without bounds
            LSlagrangecoeffs=pinv(A(ind,1:floor(c/2)));
            LSlagranges=A(1:length(x),1:floor(c/2))*LSlagrangecoeffs;
            %Calculate Lebesgue Functions with bounds
            Lebesguewithbounds=sum(abs(lagrangeswithbounds),2);
            LSLebesguewithbounds=sum(abs(LSlagrangeswithbounds),2);
            %Calculate Lebesgue Functions without bounds
            Lebesgue=sum(abs(lagranges),2);
            LSLebesgue=sum(abs(LSlagranges),2);
            %save the Nn Lebesgue constant
            LCsFeketewithbounds(1,c,i)=max(Lebesguewithbounds);
            LCsFeketeLSwithbounds(1,c,i)=max(LSLebesguewithbounds);
            LCsFekete(1,c,i)=max(Lebesgue);
            LCsFeketeLS(1,c,i)=max(LSLebesgue);
            if c==20
                %v is array of indices of x
                v=(1:length([x;xb]))';
                tonyLSwithbounds(1:c,1,i)=v(indwithbounds);
                tonyLS(1:c,1,i)=v(ind);
            end
            %Calculate Tony Lebesgue stuff with bounds
            LSlagrangecoeffsTonywithbounds=pinv(A(tonyLSwithbounds(1:c,1,i),1:floor(c/2)));
            LSlagrangesTonywithbounds=A(:,1:floor(c/2))*LSlagrangecoeffsTonywithbounds;
            LSLebesgueTonywithbounds=sum(abs(LSlagrangesTonywithbounds),2);
            [LCsTonyLSwithbounds(1,c,i),tonyLSwithbounds(c+1,1,i)]=max(LSLebesgueTonywithbounds);
            %Calculate Tony Lebesgue stuff without bounds
            LSlagrangecoeffsTony=pinv(A(tonyLS(1:c,1,i),1:floor(c/2)));
            LSlagrangesTony=A(1:length(x),1:floor(c/2))*LSlagrangecoeffsTony;
            LSLebesgueTony=sum(abs(LSlagrangesTony),2);
            [LCsTonyLS(1,c,i),tonyLS(c+1,1,i)]=max(LSLebesgueTony);
        end
        c=c+1;
    end
end
for i=1:length(shapes)
    %Comparisons with/out boundary for each shape
    figure
    semilogy(numpoints,LCsFeketewithbounds(1,:,i)*errlvl,'g');
    hold on
    semilogy(numpoints,LCsFeketeLSwithbounds(1,:,i)*errlvl,'b');
    semilogy(numpoints,LCsTonyLSwithbounds(1,:,i)*errlvl,'r');
    semilogy(numpoints,LCsFekete(1,:,i)*errlvl,'*g')
    semilogy(numpoints,LCsFeketeLS(1,:,i)*errlvl,'*b')
    semilogy(numpoints,LCsTonyLS(1,:,i)*errlvl,'*r')
    title(strcat('Lebesgue Comparisons for',' ',shapes{i}{1}))
    xlabel('Number of Points, *=without bounds, -=with bounds')
    ylabel('g-Fekete,b-FeketeLS,r-TonyLS')
    hold off
end
toc
save LebesgueOverSeveralRegionswwoutbounds1200.mat