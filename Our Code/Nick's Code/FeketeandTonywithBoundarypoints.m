allFeketepnts=800;
numpoints=1:allFeketepnts;
%Lebesgue Constants
LCsFekete=zeros(1,allFeketepnts);
LCsFeketeLS=zeros(1,allFeketepnts);
LCsTonyLS=zeros(1,allFeketepnts);
%lebesgue Constants with boundaries
LCsFeketewithbounds=zeros(1,allFeketepnts);
LCsFeketeLSwithbounds=zeros(1,allFeketepnts);
LCsTonyLSwithbounds=zeros(1,allFeketepnts);
%sup(||f-pn||) and sup(||g-pn||) over given region
maxerrorFeketeF=zeros(1,allFeketepnts);
maxerrorFeketeG=zeros(1,allFeketepnts);
maxerrorFeketeLSF=zeros(1,allFeketepnts);
maxerrorFeketeLSG=zeros(1,allFeketepnts);
maxerrorTonyLSF=zeros(1,allFeketepnts);
maxerrorTonyLSG=zeros(1,allFeketepnts);
spacing=400;
X=linspace(-4,4,spacing);
[xx,yy]=meshgrid(X);
x=xx(:);
y=yy(:);
%tonyLS is indices of Tony's approximation greedy points in x and y vectors
%needs to have one extraneous point because of the way for loop is
%written
tonyLS=zeros(allFeketepnts+1,1);
tonyLSwithbounds=zeros(allFeketepnts+1,1);
%random error to be introduced to tony points and tonyLS points
errlvl=10^-4;
errorsTonyLSF=(2*rand(allFeketepnts,1)-1)*errlvl;
errorsTonyLSG=(2*rand(allFeketepnts,1)-1)*errlvl;
% define boundary
theta = linspace(0,2*pi,1000);
xb = (1+2*cos(theta)).*cos(theta);
yb = (1+2*cos(theta)).*sin(theta);
% select points inside the boundary
ind = inpolygon(xx(:),yy(:),xb,yb);
x = x(ind);
y = y(ind);
% incirc = x.^2+y.^2 < .2^2;
% x(incirc) = [];
% y(incirc) = [];
xwithbounds=[x;xb'];
ywithbounds=[y;yb'];
%each row of A is a fine point, each column to be a basis function
A=zeros(length(xwithbounds),allFeketepnts);
%c is A's column counter
c=1;
%n is the degree of the next biggest triangular number
n=ceil((-3+sqrt(1+8*allFeketepnts))/2);
%test functions
f=@(x,y) exp(-x.^2-y.^2);
g=@(x,y) sin(2*pi*x).*cos(2*pi*y);
for i=0:n %total degree
    for j=0:i %all terms of ith power
        if c<=allFeketepnts
            %create new column of A
            if j==i
                if i==0
                    A(:,c)=ones(length(xwithbounds),1);
                else
                    A(:,c)=A(:,c-(i+1)).*ywithbounds;
                end
            else 
                A(:,c)=A(:,c-i).*xwithbounds;
            end
            %orthonormalize new column of A
            for k=1:c-1
                A(:,c)=A(:,c)-(A(:,c)'*A(:,k))*A(:,k);
            end
            A(:,c)=A(:,c)/norm(A(:,c));            
            if c==20
                b=rand(c,1);
                dummy1=A(:,1:c)'\b;
                ind1=dummy1~=0;
                dummy2=A(1:length(x),1:c)'\b;
                ind2=dummy2~=0;
                %v is array of indices of xwithbounds
                v=1:length(xwithbounds);
                tonyLSwithbounds(1:c)=v(ind1);
                tonyLS(1:c)=v(ind2);
            end
            if c>=20
                LScoeffsTonywithbounds=pinv(A(tonyLSwithbounds(1:c),1:floor(c/2)));
                LSLebesgueTonywithBounds=sum(abs(A(:,1:floor(c/2))*LScoeffsTonywithbounds),2);
                [LCsTonyLSwithbounds(c),tonyLSwithbounds(c+1)]=max(LSLebesgueTonywithBounds);
                %without boundary
                LScoeffsTony=pinv(A(tonyLS(1:c),1:floor(c/2)));
                LSLebesgueTony=sum(abs(A(1:length(x),1:floor(c/2))*LScoeffsTony),2);
                [LCsTonyLS(c),tonyLS(c+1)]=max(LSLebesgueTony);
            end
        end
        c=c+1;
    end
end
%Calculate Fekete points
b=rand(allFeketepnts,1);
dummy1=A'\b;
ind1=dummy1~=0;
dummy2=A(1:length(x),:)'\b;
ind2=dummy2~=0;
figure
plot(xwithbounds(ind1),ywithbounds(ind1),'.',xwithbounds(tonyLSwithbounds),ywithbounds(tonyLSwithbounds),'*')
title('with boundary')
figure
plot(xwithbounds(ind2),ywithbounds(ind2),'.',xwithbounds(tonyLS),ywithbounds(tonyLS),'*')