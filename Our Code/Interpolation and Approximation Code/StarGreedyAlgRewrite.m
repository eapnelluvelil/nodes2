clear;
clc;

% Construct LS approximations for the following functions
f = @(x, y) cos(3*x) + sin(2*y);
g = @(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2));

% Keep track of the max error btw. f and g and their LS approximations
max_err_f_approx_greedy = [];
max_err_g_approx_greedy = [];

% Keep track of the number of points used to construct the approximations
num_pts = [];

% Generate a discretization of the domain
N = 200;
[xx, yy, xb, yb, in] = StarNB(N);

% Rescale the points to be within [-1, 1] x [-1, 1] and make xx and yy
% column vectors
x_scale = max(max(xx));
xx = xx(:)/x_scale;

y_scale = max(max(yy));
yy = yy(:)/y_scale;

% Reshape xx and yy
xxs = reshape(xx, size(in));
yys = reshape(yy, size(in));

% Find points on and within the boundary
x_in_boundary = xx(in);
y_in_boundary = yy(in);

% Number of random points to start out with
start_val = 10;

% Number of subsequent points the greedy algorithm will compute
stop_val = 200;

% Number of basis functions the greedy alg. will use to construct the
% approximations
num_cols = floor(stop_val/2);

% Create and normalize the first column of AA
% The rows of AA will correspond to the points on and within the boundary
AA = zeros(length(xx), num_cols);
AA(:, 1) = ones(length(xx), 1);
AA(:, 1) = AA(:, 1)/norm(AA(:, 1));

% WHAT DOES THIS CODE DO?
findn = roots([1, 1, -2*num_cols]);
n = ceil(findn(findn > 0));
r = zeros(1, num_cols-1);

% Counter to track which column we're adding to AA
col = 0;

% Generate subsequent columns of AA and perform GS on them
for k = 2:n
    for j = 1:k
        col = col+1;
        if col >= num_cols
            break;
        elseif j == k
            AA(:, col+1) = AA(:, col+1-k).*yy;
        else
            AA(:, col+1) = AA(:, col+1-(k-1)).*xx;
        end
        
        for p = 1:col
            r(p) = AA(in(:), p)'*AA(in(:), col+1);
            AA(:, col+1) = AA(:, col+1) - r(p)*AA(:, p);
        end
        
        AA(:, col+1) = AA(:, col+1)/norm(AA(in(:), col+1));
    end
end

% Randomly sample start_val points on and within the boundary
start_pts_inds = randsample(find(in(:)), start_val);
greedy_x_pts = xx(start_pts_inds);
greedy_y_pts = yy(start_pts_inds);

% Initialize our LS coefficients matrix
A = zeros(stop_val, num_cols);
A(1:length(greedy_x_pts), 1:(floor(length(greedy_x_pts)/2))) = ...
    AA(start_pts_inds, 1:floor(length(greedy_x_pts)/2));

% Evaluate f and g at the candidate points
f_cand = f(x_in_boundary, y_in_boundary);
g_cand = g(x_in_boundary, y_in_boundary);

% Generate (stop_val)-(start_val) further points
while length(greedy_x_pts) < stop_val
    Ainv = pinv(A(1:length(greedy_x_pts), ...
        1:floor(length(greedy_x_pts)/2)));
    
    % Compute Lebesgue function using the current set of picked greedy
    % points
    Lebfun = 0*xx;
    Lebfun(in(:)) = sum(abs(AA(in(:), ...
        1:floor(length(greedy_x_pts)/2)))*Ainv, 2);
    Lebfun = reshape(Lebfun, size(in));
    
    % Evaluate f and g at the current set of picked greedy points
    f_greedy_pts = f(greedy_x_pts, greedy_y_pts);
    g_greedy_pts = g(greedy_x_pts, greedy_y_pts);
    
    % Compute the LS approximations to f and g
    f_greedy_approx = AA(in(:), 1:floor(length(greedy_x_pts)/2))*Ainv* ...
        f_greedy_pts;
    g_greedy_approx = AA(in(:), 1:floor(length(greedy_x_pts)/2))*Ainv* ...
        g_greedy_pts;
    
    % Compute the max error btw. f and g and their LS approximations
    max_err_f_approx_greedy = [max_err_f_approx_greedy, ...
        norm(f_cand-f_greedy_approx, 'inf')];
    max_err_g_approx_greedy = [max_err_g_approx_greedy, ...
        norm(g_cand-g_greedy_approx, 'inf')];
    
    % Store the number of picked greedy points
    num_pts = [num_pts, length(greedy_x_pts)];
    
    % Find where the Lebesgue function takes its max in the boundary
    % We pick two local maxima
    Lebfun_vec = sort(Lebfun(:));
    max_vals = Lebfun_vec(length(Lebfun_vec)-1:length(Lebfun_vec));
    hLocalMax = vision.LocalMaximaFinder;
    hLocalMax.MaximumNumLocalMaxima = 2;
    hLocalMax.NeighborhoodSize = [101 101];
    hLocalMax.Threshold = 1;
    location = step(hLocalMax, Lebfun);
    next_x_loc_1 = xxs(location(1,2), location(1,1));
    next_y_loc_1 = yys(location(1,2), location(1,1));
    next_x_loc_2 = xxs(location(2,2), location(2,1));
    next_y_loc_2 = yys(location(2,2), location(2,1));
    greedy_x_pts = [greedy_x_pts; next_x_loc_1; next_x_loc_2];
    greedy_y_pts = [greedy_y_pts; next_y_loc_1; next_y_loc_2];
    
    % Update our coefficients matrix
    start_pts_inds(length(greedy_x_pts)-1) = (sub2ind(size(xxs), ...
        location(1, 2), location(1, 1)));
    start_pts_inds(length(greedy_x_pts)) = (sub2ind(size(xxs), ...
        location(2, 2), location(2, 1)));
    
    A(length(greedy_x_pts)-1:length(greedy_x_pts), ...
        1:floor(length(greedy_x_pts)/2)-1) = ...
        AA(start_pts_inds(length(greedy_x_pts)-1:length(greedy_x_pts)), ...
        1:floor(length(greedy_x_pts)/2)-1);
    A(1:length(greedy_x_pts), floor(length(greedy_x_pts)/2)) = ...
        AA(start_pts_inds, floor(length(greedy_x_pts)/2));
end

%%
figure;
semilogy(num_pts, max_err_f_approx_greedy, 'DisplayName', ...
    'Max error btw. f and its approx. (greedy)', ...
    'LineWidth', ...
    2);
hold on;
semilogy(num_pts, max_err_g_approx_greedy, 'DisplayName', ...
    'Max error btw. g and its approx. (greedy)', ...
    'LineWidth', ...
    2);
hold off;
title("Number of points used vs. $\log(error)$", 'interpreter', ...
    'latex', 'FontSize', 15);
xlabel("Number of points used", 'interpreter', 'latex', 'FontSize', 15);
ylabel("$\log(error)$", 'interpreter', 'latex', 'FontSize', 15);
legend;