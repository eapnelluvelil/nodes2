clear;
clc;

% Construct approximants for the following functions
f = @(x, y) cos(3*x) + sin(2*y);
g = @(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2));

% To keep track of the max error btw. f and g and their LS approximants
max_err_f_approx_greedy_noise = [];
max_err_g_approx_greedy_noise = [];

% To keep track of the number of points used in the approximant
% construction
num_pts = [];

% Noise level
noise = 0;

timeto3000 = tic;
timeto1000 = tic;
timeto4000 = tic;
timeto2000 = tic;
timeto5000 = tic;
timeto6000 = tic;
timeto7000 = tic;
time3000 = [];
time1000 = [];
time4000 = [];
time2000 = [];
time5000 = [];
time6000 = [];
time7000 = [];
start=tic;

tic
[xx, yy, xb, yb, in] = StarNB(300);
meshgentime = toc;

% Rescale to get the x and y points to be within -1 and 1
xscale = max(max(xx));
yscale = max(max(yy));
xx = xx(:)/xscale;
yy = yy(:)/yscale;

% Make square matrices out of the xx and yy vectors
xxs = reshape(xx,size(in));
yys = reshape(yy,size(in));

% Find points on and within the boundary
x_in_boundary = xx(in);
y_in_boundary = yy(in);

% stopval controls how many interpolation points we end up with
stopval = 400;
% startval controls how many random interpolation points we start with
startval = 10;

% Counter to keep track of the number of columns of AA
cont = 0;

% Create and normalize the first column of AA
dnorm = norm(ones(length(xx), 1));
AA = zeros(length(xx), stopval/2);
AA(:,1) = ones(length(xx), 1)/dnorm;

%
AAholder = 0;
looptime = [];
lebmax = [];
cnumb = [];

% Number of basis functions i.e. the number of starting points divided by 2
% This is used to solve the problem in the least squares sense
numbv = floor(stopval/2);

% DON'T KNOW WHAT THIS CODE DOES
findn = roots([1, 1, -2*numbv]);
n = ceil(findn(findn > 0));
r = zeros(1, numbv-1);

% Generate subsequent columns of AA and pre-condition AA so that it will
% not be badly conditioned
for k = 2:n
    for j = 1:k
        cont = cont+1;
        if cont >= numbv
            break
        end
        if j == k
            AA(:, cont+1) = AA(:, cont+1-k).*yy;
        else
            AA(:, cont+1) = AA(:, cont+1-(k-1)).*xx;
        end
        
        for p = 1:cont
            r(p) = AA(in(:), p)'*AA(in(:), cont+1);
            AA(:, cont+1) = AA(:, cont+1)-r(p)*AA(:, p);
        end
        
        % Append the newest column of AA to AA
        dnorm(cont+1) = norm(AA(in(:), cont+1));
        AA(:, cont+1) = AA(:, cont+1)/dnorm(cont+1);
    end
end

% We sample startval points uniformly at random to kickstart our greedy
% algorithm
locholder = randsample(find(in(:)), startval);
optimal_x = xx(locholder);
optimal_y = yy(locholder);

A = zeros(stopval, stopval/2);
A(1:length(optimal_x), 1:length(optimal_x)/2) = ...
    AA(locholder, 1:length(optimal_x)/2);
Ainvtime = zeros(1, (stopval-startval)/2);
looptimetime = zeros(1, (stopval-startval)/2);
err = zeros(1, (stopval-startval)/2);
errx = zeros(1, (stopval-startval)/2);
Lebfuncgentime = zeros(1, (stopval-startval)/2);
cnumb = zeros(1, (stopval-startval)/2);
interperr = zeros(1, (stopval-startval)/2);
intererrcalctime = zeros(1, (stopval-startval)/2);
Lebmaxy = zeros(1, (stopval-startval)/2);
plotholder = cont-1;

while length(optimal_x) < stopval
    if length(optimal_x) == 12000
        time3000 = toc(timeto3000);
    elseif length(optimal_x) == 10000
        time1000 = toc(timeto1000);
    elseif length(optimal_x) == 14000
        time4000 = toc(timeto4000);
    elseif length(optimal_x) == 16000
        time2000 = toc(timeto2000);
    elseif length(optimal_x) == 18000
        time5000 = toc(timeto5000);
    elseif length(optimal_x) == 6000
        time6000 = toc(timeto6000);
    elseif length(optimal_x) == 7000
        time7000 = toc(timeto7000);
    end
    loop = tic;
    tic
    Ainv = pinv(A(1:length(optimal_x), 1:length(optimal_x)/2));
    Ainvtime(cont-plotholder) = toc;
    Leb = 0*xx;
    tic
    Leb(in(:)) = sum(abs(AA(in(:), 1:length(optimal_x)/2)*Ainv), 2);
    Leb = reshape(Leb, size(in));
    
    Lebfuncgentime(cont-plotholder) = toc;
    
    errx(cont-plotholder) = cont;
    cnumb(cont-plotholder) = cond(A(1:length(optimal_x), ...
        1:length(optimal_x)/2));
    tic
    
    interperr(cont-plotholder) = norm(sin(30*xx(in(:))).*cos(15.*yy(in(:))) ...
        -AA(in(:),1:length(optimal_x)/2)*(Ainv*(sin(30*optimal_x).*cos(15.*optimal_y))));
    
    intererrcalctime(cont-plotholder) = toc;
    
    % Evaluate f at the candidate points
    f_cand_pts = f(x_in_boundary, y_in_boundary);
    f_cand_pts = f_cand_pts(:);
    
    % Evaluate g at the candidate points
    g_cand_pts = g(x_in_boundary, y_in_boundary);
    g_cand_pts = g_cand_pts(:);
    
    % Evaluate f at the greedy alg's points and add noise
    f_greedy_pts = f(optimal_x, optimal_y);
    f_greedy_pts = f_greedy_pts(:);
    f_greedy_pts = f_greedy_pts + noise*(2*rand(size(f_greedy_pts))-1);
    
    % Evaluate g at the greedy alg's points
    g_greedy_pts = g(optimal_x, optimal_y);
    g_greedy_pts = g_greedy_pts(:);
    g_greedy_pts = g_greedy_pts + noise*(2*rand(size(g_greedy_pts))-1);
    
    % Construct our LS approximant to f using the greedy alg's points
    f_approx = AA(in(:), 1:length(optimal_x)/2)*Ainv*f_greedy_pts;
    
    % Construct our LS approximant to g using the greedy alg's points
    g_approx = AA(in(:), 1:length(optimal_x)/2)*Ainv*g_greedy_pts;
    
    % Compute max error btw. f and its LS approximant
    max_err_f_approx_greedy_noise = [max_err_f_approx_greedy_noise, ...
        max(abs(f_cand_pts) - abs(f_approx))];
    
    % Compute max error btw. g and its LS approximant
    max_err_g_approx_greedy_noise = [max_err_g_approx_greedy_noise, ...
        max(abs(g_cand_pts) - abs(g_approx))];
    
    % Store the number of points used in calculating the approximants
    num_pts = [num_pts length(optimal_x)];
    
    Lebv = sort(Leb(:));
    maxval = Lebv(length(Lebv)-1:length(Lebv));
    Lebmaxy(cont-plotholder) = maxval(2);
    hLocalMax = vision.LocalMaximaFinder;
    hLocalMax.MaximumNumLocalMaxima = 2;
    hLocalMax.NeighborhoodSize = [101 101];
    hLocalMax.Threshold = 1;
    location = step(hLocalMax, Leb);
    xxloc1= xxs(location(1,2), location(1,1));
    yyloc1 = yys(location(1,2), location(1,1));
    xxloc2 = xxs(location(2,2), location(2,1));
    yyloc2 = yys(location(2,2), location(2,1));
    optimal_x = [optimal_x; xxloc1; xxloc2];
    optimal_y = [optimal_y; yyloc1; yyloc2];
    
    
    locholder(length(optimal_x)-1) = (sub2ind(size(xxs), ...
        location(1,2), location(1,1)));
    locholder(length(optimal_x)) = (sub2ind(size(xxs), ...
        location(2,2), location(2,1)));
    A(length(optimal_x)-1:length(optimal_x),1:length(optimal_x)/2-1) = ...
        AA(locholder(length(optimal_x)-1:length(optimal_x)), ...
        1:length(optimal_x)/2-1);
    A(1:length(optimal_x),length(optimal_x)/2) = ...
        AA(locholder, length(optimal_x)/2);
    cont = cont+1;
    looptime(cont-plotholder-1) = toc(loop);
    looptime(cont-plotholder-1);
end
errx = errx-stopval/2+1;

% Rescale the points
optimal_x = optimal_x*xscale;
optimal_y = optimal_y*yscale;

%%
% Plot the # of points used in the approximant construction vs. log(error)
% btw. f and g and their LS approximants
figure;
semilogy(errx, max_err_f_approx_greedy_noise, 'DisplayName', ...
    'Max error btw. f and its approx. (greedy) with noise', ...
    'LineWidth', ...
    2);
hold on;
semilogy(errx, max_err_g_approx_greedy_noise, 'DisplayName', ...
    'Max error btw. g and its approx. (greedy) with noise', ...
    'LineWidth', ...
    2);
hold off;
title("Number of points used vs. $\log(error)$", 'interpreter', ...
    'latex', 'FontSize', 15);
xlabel("Number of points used", 'interpreter', 'latex', 'FontSize', 15);
ylabel("$\log(error)$", 'interpreter', 'latex', 'FontSize', 15);
legend;

figure;
semilogy(errx,Lebmaxy);
title('Lebesgue constant');

figure;
% Plot the nodes picked by the greedy algorithm
plot(optimal_x(startval+1:end),optimal_y(startval+1:end),'*');
hold on;
% Plot the random starting nodes
plot(optimal_x(1:startval),optimal_y(1:startval),'.');
title('nodes');

figure;
semilogy(errx,Lebfuncgentime);
hold on;
semilogy(errx,intererrcalctime,'r');
hold on;
semilogy(errx,Ainvtime,'g');
hold on;
semilogy(errx,looptime,'b');
title('Time to calculate');
legend('Calculate Lebesgue Function','Interpolation Error', ...
    'Calculate Ainv','Looptime');

figure
semilogy(errx,cnumb, 'k')
hold on
semilogy(errx,interperr)
title('Condition number of A')

totaltime=toc(start);

% save('LData','Lebmaxy','optimal_x', 'optimal_y', 'Lebfuncgentime', ...
% 'intererrcalctime', 'Ainvtime', 'looptime', 'cnumb', 'interperr', ...
%     'time3000','time1000','time4000','time2000','time5000','time6000', ...
%     'time7000','totaltime');

% Save the number of points used and the approximation errors
save('GreedyDataNoise', 'num_pts', 'max_err_f_approx_greedy_noise', ...
    'max_err_g_approx_greedy_noise');