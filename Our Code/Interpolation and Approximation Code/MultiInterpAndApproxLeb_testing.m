% To analyze our results over multiple runs
rng(0);

% Set how much noise we want to introduce when
% inteprolating and approximating
noise = 1e-7;

% Specify the sides of the polygon shaped domains
sides = 3:10;

%% Generate discretization of our domain
for side_idx = 1:length(sides)
    N = 300;
    n = sides(side_idx);
    tic;
    [xb, yb, xx, yy, in] = PolygonPointGen(N, n);
    fprintf("Time took to generate meshgrid: %f\n", toc);

    % Select the points that lie on and/or within
    % the domain boundary
    x_bound = xx(in);
    % x_bound = [x_bound; xb(:)];
    y_bound = yy(in);
    % y_bound = [y_bound; yb(:)];

    % Plot the domain
    figure;
    plot(xb, yb);
    title(strcat(num2str(n), " sided polygon domain"));

    %% Specify the number of points to use for the QR algorithm

    num_interp_pts = 200;

    find_max_tot_deg_interp = roots([1, 1, -2*num_interp_pts]);
    max_tot_deg_interp = ceil(find_max_tot_deg_interp(...
        find_max_tot_deg_interp > 0));
    num_cols_interp = max_tot_deg_interp*(max_tot_deg_interp+1)/2;

    %% Functions to interpolate and approximate

    funcs = {@(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2)), ...
        @(x, y) (exp(x+y)), ...
        @(x, y) (exp(x.*y)), ...
        };

    %% Track interpolation and approximation errors for QR algorithm

    errs_interp_qr = cell(size(funcs));
    for idx = 1:length(errs_interp_qr)
        errs_interp_qr{idx} = zeros(1, num_cols_interp/2); 
    end

    errs_approx_qr = cell(size(funcs));
    for idx = 1:length(errs_approx_qr)
        errs_approx_qr{idx} = zeros(1, num_cols_interp/2);
    end

    %% Track Lebesgue constants for QR algorithm
    lebconsts_interp_qr = zeros(1, num_cols_interp/2);
    lebconsts_approx_qr = zeros(1, num_cols_interp/2);

    %% Generate Vandermonde matrix for column-pivoting QR algorithm

    tic;
    Vqr = zeros(length(x_bound), num_cols_interp);

    % Counter to keep track of the current column
    colqr = 0;

    % Generate columns of the matrix
    for k = 0:max_tot_deg_interp
        for j = 0:max_tot_deg_interp
            % Generate the next basis function if its degree 
            % is less than the max total degree for interpolation
            if (j+k) < max_tot_deg_interp
                Vqr(:, colqr+1) = cos(k*acos(x_bound)).*cos(j*acos(y_bound));

                % Make the new basis functions orthogonal to the previous
                % functions
                for p = 1:colqr
                    % Find the component of the new basis function in the 
                    % direction of the previous functions
                    comp = Vqr(:, colqr+1)'*Vqr(:, p);
                    Vqr(:, colqr+1) = Vqr(:, colqr+1)-comp*Vqr(:, p);
                end

                % Normalize the new basis function
                col_norm = norm(Vqr(:, colqr+1));
                Vqr(:, colqr+1) = Vqr(:, colqr+1)/col_norm;

                % Update the column idex
                colqr = colqr+1;

            end
        end
    end
    fprintf('Time took to create Vandermonde matrix for QR algorithm: %f\n', ...
        toc);
    %% Run column-pivoting QR algorithm

    tic
    for numpts = 2:2:num_cols_interp
        % Create a submatrix of the Vandermonde matrix to
        % find approximate Fekete points (AFPs)
        Vafps = Vqr(:, 1:numpts);

        % Find AFPs
        rand_b = rand(size(Vafps, 2), 1);
        afps_locs = Vafps'\rand_b;
        afps_locs = (afps_locs ~= 0);

        xafps = x_bound(afps_locs);
        yafps = y_bound(afps_locs);

        % Number of points to use for LS approximation
        numpts_approx = numpts/2;

        % Find the Lagrange function coefficients for interpolation
        lagrange_coeffs_interp = ...
            Vafps(afps_locs, :)\eye(size(Vafps(afps_locs, :)));

        lagrange_funcs_interp = Vafps*lagrange_coeffs_interp;

        % Compute the Lebesgue constant using the
        % interpolation points
        lebfun_interp_qr = sum(abs(lagrange_funcs_interp), 2);
        lebconsts_interp_qr(numpts/2) = max(lebfun_interp_qr);

        % Find the Lagrange function coefficients for approximation
        lagrange_coeffs_approx = ...
            Vafps(afps_locs, 1:numpts_approx)\eye(size(Vafps(afps_locs, :)));

        lagrange_funcs_approx = ...
            Vafps(:, 1:numpts_approx)*lagrange_coeffs_approx;

        % Compute the Lebesgue constant using the 
        % LS approximation points
        lebfun_approx_qr = sum(abs(lagrange_funcs_approx), 2);
        lebconsts_approx_qr(numpts/2) = max(lebfun_approx_qr);

        % Construct interpolants and approximants
        for idx = 1:length(funcs)
            % Evaluate the function at the AFPs and add noise
            fafps = funcs{idx}(xafps, yafps);
            fafps = fafps(:);
            fafps = fafps + noise*(2*rand(size(fafps))-1);

            % Evaluate the function at all the domain points
            fdom = funcs{idx}(x_bound, y_bound);
            fdom = fdom(:);

            % Construct the interpolant
            finterp = lagrange_funcs_interp*fafps;

            % Compute the max error between the interpolant 
            % and the function
            errs_interp_qr{idx}(numpts/2) = norm(fdom-finterp, 'inf');

            % Construct the approximant
            fapprox = lagrange_funcs_approx*fafps;

            % Compute the max error between the approximant
            % and the function
            errs_approx_qr{idx}(numpts/2) = norm(fdom-fapprox, 'inf');
        end
    end
    fprintf('Time took to run QR algorithm: %f\n', toc);

    %% Plot results for column-pivoting QR algorithm

    % Specify the number of points we used for interpolation
    % and approximation
    num_pts_interp = 2:2:num_cols_interp;
    num_pts_approx = num_pts_interp/2;

    % Plot the max errors for the interpolants
    figure;
    for idx = 1:length(funcs)
        semilogy(num_pts_interp, errs_interp_qr{idx}, ...
            "LineWidth", 2);
        hold on;
    end
    semilogy(num_pts_interp, lebconsts_interp_qr, ...
        "LineWidth", 2);
%     title("Number of points used vs. $\log(error)$ (Interpolation, QR)", ...
%         "FontSize", 15);
    title(strcat(num2str(n), " - Number of points used vs $\log(error)$ (interp, QR)"), ...
        "interpreter", "latex", "FontSize", 15);
    xlabel("Number of points used", ...
        "interpreter", "latex", ...
        "FontSize", 15);
    ylabel("$\log(error)$", ...
        "interpreter", "latex", ...
        "FontSize", 15);

    % Plot the Lebesgue constants associated with interpolation
    % figure;
    % semilogy(num_pts_interp, lebconsts_interp_qr, ...
    %     "LineWidth", 2);
    % title("Number of points used vs. $\Lambda$ (Interpolation, QR)", ...
    %     "interpreter", "latex", "FontSize", 15);
    % xlabel("Number of points used", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);
    % ylabel("$\Lambda$", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);

    % Plot the max errors for the approximants
    figure;
    for idx = 1:length(funcs)
        semilogy(num_pts_approx, errs_approx_qr{idx}, ...
            "LineWidth", 2);
        hold on;
    end
    semilogy(num_pts_approx, lebconsts_approx_qr, ...
        "LineWidth", 2);
    title(strcat(num2str(n), " - Number of points used vs $\log(error)$ (approx, QR)"), ...
        "interpreter", "latex", "FontSize", 15);
    xlabel("Number of points used", ...
        "interpreter", "latex", ...
        "FontSize", 15);
    ylabel("$\log(error)$", ...
        "interpreter", "latex", ...
        "FontSize", 15);

    % Plot the Lebesgue constants associated with approximation
    % figure;
    % semilogy(num_pts_approx, lebconsts_approx_qr, ...
    %     "LineWidth", 2);
    % title("Number of points used vs. $\Lambda$ (LS Approximation, QR)", ...
    %     "interpreter", "latex", "FontSize", 15);
    % xlabel("Number of points used", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);
    % ylabel("$\Lambda$", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);

    %% Specify the number of points to use for Tony's algorithm

    % How many random points we initially sample
    start_val = 10;

    % How many points we want to ultimately compute
    stop_val = 2*num_interp_pts;

    % Find how many columns the Vandermonde matrix will have
    num_cols = stop_val/2;

    % Find the max total degree of the Vandermonde matrix's columns
    find_tot_deg = roots([1, 1, -2*num_cols]);
    tot_deg = ceil(find_tot_deg(find_tot_deg > 0));

    % Track the number of points we'll use to
    % construct approximations
    numptsg = zeros(1, (stop_val-start_val)/2);

    %% Track approximation errors for Tony's algorithm

    % Track the max approx. error btw. functions
    % and their approximants
    max_approx_err_f1_g = zeros(1, (stop_val-start_val)/2);
    errs_approx_g = cell(size(funcs));
    for idx = 1:length(errs_approx_g)
        errs_approx_g{idx} = zeros(1, (stop_val-start_val)/2);
    end

    %% Track Lebesgue constants for Tony's algorithm

    % Store the Lebesgue constants
    lebconsts_approx_g = zeros(1, (stop_val-start_val)/2);

    %% Generate Vandermonde matrix for Tony's algorithm

    tic;
    % Generate the Vandermonde matrix
    Vg = zeros(length(xx(:)), stop_val/2);
    Vg(:, 1) = ones(length(xx(:)), 1)/norm(ones(length(xx(:)), 1));

    % Track which column we're working with
    colg = 0;
    for k = 2:tot_deg
        for j = 1:k
            % Update the column index
            colg = colg+1;

            if colg >= num_cols
                break;
            elseif j == k
                Vg(:, colg+1) = Vg(:, colg+1-k).*yy(:);
            else
                Vg(:, colg+1) = Vg(:, colg+1-(k-1)).*xx(:);
            end

            % Orthogonalize the columns
            for p = 1:colg
                comp = Vg(in(:), p)'*Vg(in(:), colg+1);
                Vg(:, colg+1) = Vg(:, colg+1)-comp*Vg(:, p);
            end

            % Normalize the columns
            col_norm = norm(Vg(in(:), colg+1));
            Vg(:, colg+1) = Vg(:, colg+1)/col_norm;
        end
    end
    fprintf("Time took to create Vandermonde matrix for Tony's algorithm: %f\n", ...
        toc);

    %% Run Tony's algorithm

    tic;
    % Sample some random points within the domain
    locs = randsample(find(in(:)), start_val);
    xopt = xx(locs);
    yopt = yy(locs);

    % Initialize the matrix for finding the Lagrange function
    % coefficients
    V = zeros(stop_val, stop_val/2);
    V(1:length(xopt), 1:length(xopt)/2) = ...
        Vg(locs, 1:length(xopt)/2);

    % Track the update index
    update_idx = colg-1;

    % Generate points until we have more points than desired
    while length(xopt) < stop_val
        % Find the coefficients of the Lagrange functions
        Vinv = pinv(V(1:length(xopt), 1:length(xopt)/2));

        % Generate the Lebesgue function
        lebfun = 0*xx(:);
        lebfun(in(:)) = sum(abs(Vg(in(:), 1:length(xopt)/2)*Vinv), 2);
        lebfun = reshape(lebfun, size(in));

        % Track the number of points we've used
        numptsg(colg-update_idx) = colg;

        % Construct approximations to the functions and compute
        % the max error
        for idx = 1:length(funcs)
            % Evaluate the function at all the domain points
            fdom = funcs{idx}(xx(in), yy(in)); fdom = fdom(:);

            % Evaluate the function at the optimal points
            % and add noise
            fg = funcs{idx}(xopt, yopt); fg = fg(:);
            fg = fg + noise*(2*rand(size(fg))-1);

            % Construct our approximation
            fapprox = Vg(in(:), 1:length(xopt)/2)*Vinv*fg;

            % Compute the max error between the function
            % and the approximation
            errs_approx_g{idx}(colg-update_idx) = norm(fdom-fapprox, 'inf');
        end

        % Find two local maxima of the Lebesgue function
        lebv = sort(lebfun(:));
        lebconsts_approx_g(colg-update_idx) = lebv(end);

        hLocalMax = vision.LocalMaximaFinder;
        hLocalMax.MaximumNumLocalMaxima = 2;
        hLocalMax.NeighborhoodSize = [101, 101];
        hLocalMax.Threshold = 1;

        optlocs = step(hLocalMax, lebfun);

        xxloc1 = xx(optlocs(1, 2), optlocs(1, 1));
        yyloc1 = yy(optlocs(1, 2), optlocs(1, 1));
        xxloc2 = xx(optlocs(2, 2), optlocs(2, 1));
        yyloc2 = yy(optlocs(2, 2), optlocs(2, 1));

        xopt = [xopt; xxloc1; xxloc2];
        yopt = [yopt; yyloc1; yyloc2];

        % Update the locations of the optimal points
        locs(length(xopt)-1) = sub2ind(size(in), ...
            optlocs(1, 2), optlocs(1, 1));
        locs(length(xopt)) = sub2ind(size(in), ...
            optlocs(2, 2), optlocs(2, 1));

        % Update the matrix used to find Lagrange coefficients
        V(length(xopt)-1:length(xopt), 1:(length(xopt)/2)-1) = ...
            Vg(locs(length(xopt)-1:length(xopt)), ...
                1:(length(xopt)/2)-1);
        V(1:length(xopt), length(xopt)/2) = ...
            Vg(locs, length(xopt)/2);

        colg = colg+1;
    end
    fprintf("Time took to run Tony's algorithm: %f\n", toc);
    fprintf("\n");

    % Rescale the vector of used points to get 
    % the right number of points
    % (Why do we need this?)
    numptsg = numptsg - (stop_val/2) + 1;

    %% Plot results from Tony's algorithm

    % Plot max approximation errors 
    figure;
    for idx = 1:length(funcs)
        semilogy(numptsg, errs_approx_g{idx}, ...
            'LineWidth', 2);
        hold on;
    end
    semilogy(numptsg, lebconsts_approx_g, ...
        'LineWidth', 2);
    title(strcat(num2str(n), " - Number of points used vs $\log(error)$ (approx, Tony)"), ...
        "interpreter", "latex", "FontSize", 15);
    xlabel("Number of points used", ...
        "interpreter", "latex", ...
        "FontSize", 15);
    ylabel("$\log(error)$", ...
        "interpreter", "latex", ...
        "FontSize", 15);

    % Plot Lebesgue constants
    % figure;
    % semilogy(numptsg, lebconsts_approx_g, ...
    %     'LineWidth', 2);
    % title("Number of points used vs. $\Lambda$ (LS Approximation, Tony)", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);
    % xlabel("Number of points used", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);
    % ylabel("$\Lambda$", ...
    %     "interpreter", "latex", ...
    %     "FontSize", 15);
end