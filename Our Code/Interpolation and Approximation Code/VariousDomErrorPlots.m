% Code that generates interpolation and LS approximation
% error plots over the following domains:
%   - Disk
%   - Unit square
%   - Rectangle
%   - Triangle
%   - L-shaped region
%   - Star-region
%   - All the above polygons with circles cut out of them
