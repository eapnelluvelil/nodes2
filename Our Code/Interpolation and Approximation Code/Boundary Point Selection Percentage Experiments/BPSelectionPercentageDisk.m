% Investigate how the column-pivoting QR algorithm and the 
% greedy-add algorithm pick boundary points in comparison to
% interior points over the disk in the square [-1, 1]^2
clc;
clear;

% Plotting flags
DN = 'DisplayName';
LW = 'LineWidth';
L = 'Location';

%% Generate disk domain
% Specify how fine we want our meshgrid to be
N = 300;

[xx, yy] = meshgrid(linspace(-1, 1, N));

theta = linspace(0, 2*pi, 3*N);

xbdisk = cos(theta);
ybdisk = sin(theta);
indisk = inpolygon(xx, yy, xbdisk, ybdisk);

% Index separating interior points from boundary points
bind = length(xx(indisk));

% Generate candidate points
x = [xx(indisk); xbdisk(:)];
y = [yy(indisk); ybdisk(:)];

% Plot the domain
figure;
plot(xbdisk, ybdisk, '.');
title('Disk');
xlabel('x');
ylabel('y');
axis image;

%% Specify interpolant total degrees
% We're interested in picking upto as many interpolation points as there
% are boundary points
tds = 0:30;

%% Track percentage of boundary points selected
numptsqr = zeros(length(tds), 1);
numptsqr_b = zeros(length(tds), 1);

numptsg = zeros(length(tds), 1);
numptsg_b = zeros(length(tds), 1);

%% Create Vandermonde matrix
V = zeros(length(x), nchoosek(tds(end)+2, 2));

c = 0;
for k = 0:tds(end)
    for j = 0:tds(end)
        if (j+k) <= tds(end)
            V(:, c+1) = cos(k*acos(x)).*cos(j*acos(y));
            
            for p = 1:c
                comp = V(:, p)'*V(:, c+1);
                V(:, c+1) = V(:, c+1)-comp*V(:, p);
            end
            
            colnorm = norm(V(:, c+1));
            V(:, c+1) = V(:, c+1)/colnorm;
            
            c = c+1;
        end
    end
end

%% Keep track of the AFP's and GA points
afps_locs = cell(length(tds), 1);
gpts_locs = cell(length(tds), 1);

for idx = 1:length(tds)
    afps_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    gpts_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
end

%% To kickstart the greedy-add algorithm
numstartpts = 1;

if numstartpts <= length(gpts_locs{1})
    gpts_locs{1}(1:numstartpts) = randsample(find(ones(length(x), 1)), ...
    numstartpts);
else
    gpts_locs{1} = randsample(find(ones(length(x))), ...
        numstartpts-length(gpts_locs{1}));
end

totpts = numstartpts;

%% Run the column-pivoting QR algorithm and the greedy-add algorithm
for idx = 1:length(tds)
    td = tds(idx);
    
    Vtemp = V(:, 1:nchoosek(td+2, 2));
    
    % Perform the column-pivoting QR algorithm to find AFP's
    randb = rand(nchoosek(td+2, 2), 1);
    afps = Vtemp'\randb;
    afps = find(afps);
    afps_locs{idx} = afps;
    
    % Find the number of AFP's that are boundary points compared to the
    % number of total interpolation points
    numptsqr(idx) = 100*length(find(afps > bind))/...
        length(afps);
    
    % Find the number of AFP's that are boundary points compared to the
    % number of total boundary points
    numptsqr_b(idx) = 100*length(find(afps > bind))/length(xbdisk);
    
    % Perform the greedy-add algorithm to find greedy-add points
    while totpts < length(gpts_locs{idx})
        Vinv = pinv(V(gpts_locs{idx}(1:totpts), 1:totpts));
        
        % Compute the Lebesgue function
        lebfun = sum(abs(V(:, 1:totpts)*Vinv), 2);
        
        % Find the Lebesgue constant
        [lebconst, lebconstarg] = max(lebfun);
        
        % Append the location of the newest greedy-add points
        gpts_locs{idx}(totpts+1) = lebconstarg;
        
        % Update the number of points we have
        totpts = totpts+1;
    end
    
    % Populate the vector that stores the locations of the greedy-add
    % points for the next total degree with the current vector of
    % greedy-add point locations
    if td < tds(end)
        gpts_locs{idx+1}(1:totpts) = gpts_locs{idx};
    end
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to all the greedy-add points
    numptsg(idx) = 100*length(find(gpts_locs{idx} > bind))/...
        totpts;
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to the number of all boundary points
    numptsg_b(idx) = 100*length(find(gpts_locs{idx} > bind))/length(xbdisk);
end

%% Plot the results
% Plot distribution of boundary and interior AFP's on disk
h_afp = figure;
filename_afp = 'animated_AFP_Disk.gif';

for idx = 1:length(tds)
    b_afps_locs = afps_locs{idx}(afps_locs{idx} > bind);
    int_afps_locs = afps_locs{idx}(afps_locs{idx} <= bind);
    plot(x(b_afps_locs), y(b_afps_locs), 'b.', ...
        x(int_afps_locs), y(int_afps_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis square;
    title('Distribution of boundary and interior AFPs on disk');
    xlabel('x');
    ylabel('y');
    drawnow;
    
    % Capture plot as image
    frame = getframe(h_afp);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    
    % Write to the GIF file
    if idx == 1
        imwrite(imind, cm, filename_afp, 'gif', 'LoopCount', inf);
    else
        imwrite(imind, cm, filename_afp, 'gif', 'WriteMode', 'append');
    end
end


% Plot distribution of boundary and interior GA points on disk
h_ga = figure;
filename_ga = 'animated_GA_Disk.gif';

for idx = 1:length(tds)
    b_gpts_locs = gpts_locs{idx}(gpts_locs{idx} > bind);
    int_gpts_locs = gpts_locs{idx}(gpts_locs{idx} <= bind);
    plot(x(b_gpts_locs), y(b_gpts_locs), 'b.', ...
        x(int_gpts_locs), y(int_gpts_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis square;
    title('Distribution of boundary and interior GA points on disk');
    xlabel('x');
    ylabel('y');
    drawnow;
    
    % Capture plot as image
    frame = getframe(h_ga);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    
    % Write to the GIF file
    if idx == 1
        imwrite(imind, cm, filename_ga, 'gif', 'LoopCount', inf);
    else
        imwrite(imind, cm, filename_ga, 'gif', 'WriteMode', 'append');
    end
end

% figure;
% plot(tds, numptsqr, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total selected pts. (AFP)');
% hold on;
% plot(tds, numptsg, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total selected pts. (greedy-add points)');
% title('Disk domain - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
% xlabel('Total degree');
% ylabel('Percentage of boundary points picked divided by total selected pts.');
% ylim([0, 100]);
% legend(L, 'best');
% 
% figure;
% plot(tds, numptsqr_b, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total boundary pts. (AFP)');
% hold on;
% plot(tds, numptsg_b, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total boundary pts. (greedy-add pts)');
% title('Disk domain - Total degree vs. percentage of boundary pts. picked divided by total boundary pts.');
% xlabel('Total degree');
% ylabel('Percentage of boundary pts. picked divided by total boundary pts.');
% ylim([0, 100]);
% legend(L, 'best');