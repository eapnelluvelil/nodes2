% Investigate how the column-pivoting QR algorithm and the 
% greedy-add algorithm pick boundary points in comparison to
% interior points over an equilateral triangle within the square
% [-1, 1]^2
clc;
clear;

% Plotting flags
DN = 'DisplayName';
LW = 'LineWidth';
L = 'Location';

% Specify how fine we want our meshgrid to be
N = 300;
% spc = 1/N;
[xx, yy] = meshgrid(linspace(-1, 1, N));

%% Generate equilateral triangle domain
% Discretization of triangle using equispaced nodes
% xbt1 = -1:spc:1;
% ybt1 = -1*ones(1, length(xbt1));
% xbt2 = -1:spc:0;
% ybt2 = 2*xbt2+1;
% xbt3 = 0:spc:1;
% ybt3 = -2*xbt3+1;
% 
% xbtri = [xbt1, xbt2, xbt3]';
% ybtri = [ybt1, ybt2, ybt3]';

% Discretization of triangle using Chebyshev nodes
xbtri1 = chebpts(N);
ybtri1 = -ones(length(xbtri1), 1);

a = 1;
b = 0;
xbtri2 = chebpts(N);
xbtri2 = (1/2)*(a+b) + (1/2)*(b-a)*xbtri2;
ybtri2 = -2*xbtri2 + 1;

a = 0;
b = -1;
xbtri3 = chebpts(N);
xbtri3 = (1/2)*(a+b) + (1/2)*(b-a)*xbtri3;
ybtri3 = 2*xbtri3 + 1;

xbtri = [xbtri1; xbtri2; xbtri3];
ybtri = [ybtri1; ybtri2; ybtri3];
intri = inpolygon(xx, yy, xbtri, ybtri);

% Index separating interior points from boundary points
bind = length(xx(intri));

% Generate candidate points
x = [xx(intri); xbtri(:)];
y = [yy(intri); ybtri(:)];

% Plot the domain
figure;
plot(xbtri, ybtri, '.');
% title('Equilateral triangle - Equispaced node boundary discretization');
title('Equilateral triangle - Chebyshev node boundary discretization');
xlabel('x');
ylabel('y');
axis image;

%% Specify interpolant total degrees
% We're interested in picking upto as many interpolation points as there
% are boundary points
tds = 0:30;

%% Track percentage of boundary points selected
numptsqr = zeros(length(tds), 1);
numptsqr_b = zeros(length(tds), 1);

numptsg = zeros(length(tds), 1);
numptsg_b = zeros(length(tds), 1);

%% Create Vandermonde matrix
V = zeros(length(x), nchoosek(tds(end)+2, 2));

c = 0;
for k = 0:tds(end)
    for j = 0:tds(end)
        if (j+k) <= tds(end)
            V(:, c+1) = cos(k*acos(x)).*cos(j*acos(y));
            
            for p = 1:c
                comp = V(:, p)'*V(:, c+1);
                V(:, c+1) = V(:, c+1)-comp*V(:, p);
            end
            
            colnorm = norm(V(:, c+1));
            V(:, c+1) = V(:, c+1)/colnorm;
            
            c = c+1;
        end
    end
end

%% Keep track of the AFP's and GA points
afps_locs = cell(length(tds), 1);
gpts_locs = cell(length(tds), 1);

for idx = 1:length(tds)
    afps_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    gpts_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
end

%% To kick-start the greedy-add algorithm
numstartpts = 1;

if numstartpts <= length(gpts_locs{1})
    gpts_locs{1}(1:numstartpts) = randsample(find(ones(length(x), 1)), ...
    numstartpts);
else
    gpts_locs{1} = randsample(find(ones(length(x))), ...
        numstartpts-length(gpts_locs{1}));
end

totpts = numstartpts;

%% Run the column-pivoting QR algorithm and the greedy-add algorithm
for idx = 1:length(tds)
    td = tds(idx);
    
    Vtemp = V(:, 1:nchoosek(td+2, 2));
    
    % Perform the column-pivoting QR algorithm to find AFP's
    randb = rand(nchoosek(td+2, 2), 1);
    afps = Vtemp'\randb;
    afps = find(afps);
    afps_locs{idx} = afps;
    
    % Find what percentage of the AFP's are boundary points compared
    % to the number of selected interpolation points
    numptsqr(idx) = 100*length(find(afps > bind))/...
        length(afps);
    
    % Find the number of selected AFP's that are boundary points compared
    % to the number of selected AFP's
    numptsqr_b(idx) = 100*length(find(afps > bind))/...
        length(xbtri);
    
    % Perform the greedy-add algorithm to find greedy-add points
    while totpts < length(gpts_locs{idx})
        Vinv = pinv(V(gpts_locs{idx}(1:totpts), 1:totpts));
        
        % Compute the Lebesgue function
        lebfun = sum(abs(V(:, 1:totpts)*Vinv), 2);
        
        % Find the Lebesgue constant
        [lebconst, lebconstarg] = max(lebfun);
        
        % Append the location of the newest greedy-add points
        gpts_locs{idx}(totpts+1) = lebconstarg;
        
        % Update the number of points we have
        totpts = totpts+1;
    end
    
    % Populate the vector that stores the locations of the greedy-add
    % points for the next total degree with the current vector of
    % greedy-add point locations
    if td < tds(end)
        gpts_locs{idx+1}(1:totpts) = gpts_locs{idx};
    end
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to all the greedy-add points
    numptsg(idx) = 100*length(find(gpts_locs{idx} > bind))/...
        totpts;
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to the total number of boundary points
    numptsg_b(idx) = 100*length(find(gpts_locs{idx} > bind))/...
        length(xbtri);
end

%% Plot the results
% Plot distribution of boundary and interior AFP's on equilateral triangle
figure;
for idx = 1:length(tds)
    b_afps_locs = afps_locs{idx}(afps_locs{idx} > bind);
    int_afps_locs = afps_locs{idx}(afps_locs{idx} <= bind);
    plot(x(b_afps_locs), y(b_afps_locs), 'b.', ...
        x(int_afps_locs), y(int_afps_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis image;
    title('Distribution of boundary and interior AFPs on equilateral triangle');
    xlabel('x');
    ylabel('y');
    pause(0.25);
end

% Plot distribution of boundary and interior GA points on equilateral
% triangle
figure;
for idx = 1:length(tds)
    b_gpts_locs = gpts_locs{idx}(gpts_locs{idx} > bind);
    int_gpts_locs = gpts_locs{idx}(gpts_locs{idx} <= bind);
    plot(x(b_gpts_locs), y(b_gpts_locs), 'b.', ...
        x(int_gpts_locs), y(int_gpts_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis image;
    title('Distribution of boundary and interior GA points on equilateral triangle');
    xlabel('x');
    ylabel('y');
    pause(0.25); 
end

figure;
plot(tds, numptsqr, LW, 2, DN, ...
    'Percentage of boundary pts. picked divided by total selected pts. (AFP)');
hold on;
plot(tds, numptsg, LW, 2, DN, ...
    'Percentage of boundary pts. picked divided by total selected pts. (greedy-add points)');
%title('Triangle domain (Equispaced node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
title('Triangle domain (Chebyshev node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
xlabel('Total degree');
ylabel('Percentage of boundary points picked divided by total selected pts.');
ylim([0, 100]);
legend(L, 'best');

figure;
plot(tds, numptsqr_b, LW, 2, DN, ...
    'Percentage of boundary pts. picked divided by total boundary pts. (AFP)');
hold on;
plot(tds, numptsg_b, LW, 2, DN, ...
    'Percentage of boundary pts. picked divided by total boundary pts. (greedy-add pts)');
%title('Triangle domain (Equispaced node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
title('Triangle domain (Chebyshev node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total boundary pts.');
xlabel('Total degree');
ylabel('Percentage of boundary pts. picked divided by total boundary pts.');
ylim([0, 100]);
legend(L, 'best');