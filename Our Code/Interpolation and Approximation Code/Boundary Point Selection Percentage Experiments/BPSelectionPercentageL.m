% Investigate how the column-pivoting QR algorithm and the 
% greedy-add algorithm pick boundary points in comparison to
% interior points on the L shaped domain in the square [-1, 1]^2
clc;
clear;

% Plotting flags
DN = 'DisplayName';
LW = 'LineWidth';
L = 'Location';

% Specify how fine we want our meshgrid to be
N = 300;
% spc = 1/N;
[xx, yy] = meshgrid(linspace(-1, 1, N));

%% Generate L-shaped domain
% Discretization using equispaced nodes
% xbl1 = -1:spc:1;
% ybl1 = -ones(1, length(xbl1));
% 
% xbl2 = ones(1, length(-1:spc:0));
% ybl2 = -1:spc:0;
% 
% xbl3 = 0:spc:1;
% ybl3 = 0*xbl3;
% 
% xbl4 = zeros(1, length(0:spc:1));
% ybl4 = 0:spc:1;
% 
% xbl5 = 0:-spc:-1;
% ybl5 = ones(1, length(xbl5));
% 
% xbl6 = -ones(1, length(-1:spc:1));
% ybl6 = 1:-spc:-1;
% 
% xbl = [xbl1, xbl2, xbl3, xbl4, xbl5, xbl6]';
% ybl = [ybl1, ybl2, ybl3, ybl4, ybl5, ybl6]';

% Discretization using Chebyshev nodes
xbl1 = chebpts(N);
ybl1 = -ones(length(xbl1), 1);

a = -1;
b = 0;
ybl2 = chebpts(0.5*N);
ybl2 = (1/2)*(a+b) + (1/2)*(b-a)*ybl2;
xbl2 = ones(length(ybl2), 1);

a = 1;
b = 0;
xbl3 = chebpts(0.5*N);
xbl3 = (1/2)*(a+b) + (1/2)*(b-a)*xbl3;
ybl3 = zeros(length(xbl3), 1);

a = 0;
b = 1;
ybl4 = chebpts(0.5*N);
ybl4 = (1/2)*(a+b) + (1/2)*(b-a)*ybl4;
xbl4 = zeros(length(ybl4), 1);

a = 0;
b = -1;
xbl5 = chebpts(0.5*N);
xbl5 = (1/2)*(a+b) + (1/2)*(b-a)*xbl5;
ybl5 = ones(length(xbl5), 1);

a = 1;
b = -1;
ybl6 = chebpts(N);
ybl6 = (1/2)*(a+b) + (1/2)*(b-a)*ybl6;
xbl6 = -ones(length(ybl6), 1);

xbl = [xbl1; xbl2; xbl3; xbl4; xbl5; xbl6];
ybl = [ybl1; ybl2; ybl3; ybl4; ybl5; ybl6];
inl = inpolygon(xx, yy, xbl, ybl);

% Index separating interior points from boundary points
bind = length(xx(inl));

% Generate candidate points
x = [xx(inl); xbl(:)];
y = [yy(inl); ybl(:)];

% Plot the domain
figure;
plot(xbl, ybl, '.');
title('L-shaped domain - Chebyshev node boundary discretization');
xlabel('x');
ylabel('y');
axis image;

%% Specify interpolant total degrees
% We're interested in picking upto as many interpolation points as there
% are boundary points
tds = 0:30;

%% Track percentage of boundary points selected
numptsqr = zeros(length(tds), 1);
numptsqr_b = zeros(length(tds), 1);

numptsg = zeros(length(tds), 1);
numptsg_b = zeros(length(tds), 1);

%% Create Vandermonde matrix
V = zeros(length(x), nchoosek(tds(end)+2, 2));

c = 0;
for k = 0:tds(end)
    for j = 0:tds(end)
        if (j+k) <= tds(end)
            V(:, c+1) = cos(k*acos(x)).*cos(j*acos(y));
            
            for p = 1:c
                comp = V(:, p)'*V(:, c+1);
                V(:, c+1) = V(:, c+1)-comp*V(:, p);
            end
            
            colnorm = norm(V(:, c+1));
            V(:, c+1) = V(:, c+1)/colnorm;
            
            c = c+1;
        end
    end
end

%% Keep track of AFP's and GA points
afps_locs = cell(length(tds), 1);
gpts_locs = cell(length(tds), 1);

for idx = 1:length(tds)
    afps_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    gpts_locs{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
end

%% To kickstart the GA algorithm
numstartpts = 1;

if numstartpts <= length(gpts_locs{1})
    gpts_locs{1}(1:numstartpts) = randsample(find(ones(length(x), 1)), ...
    numstartpts);
else
    gpts_locs{1} = randsample(find(ones(length(x))), ...
        numstartpts-length(gpts_locs{1}));
end

totpts = numstartpts;

%% Run the column-pivoting QR algorithm and the greedy-add algorithm
for idx = 1:length(tds)
    td = tds(idx);
    
    Vtemp = V(:, 1:nchoosek(td+2, 2));
    
    % Perform the column-pivoting QR algorithm to find AFP's
    randb = rand(nchoosek(td+2, 2), 1);
    afps = Vtemp'\randb;
    afps = find(afps);
    afps_locs{idx} = afps;
    
    % Find the number of selected AFP's that are boundary points compared
    % to the number of selected AFP's
    numptsqr(idx) = 100*length(find(afps > bind))/...
        length(afps);
    
    % Find the number of selected AFP's that are boundary points compared
    % to the number of total boundary points
    numptsqr_b(idx) = 100*length(find(afps > bind))/...
        length(xbl);
    
    % Perform the greedy-add algorithm to find greedy-add points
    while totpts < length(gpts_locs{idx})
        Vinv = pinv(V(gpts_locs{idx}(1:totpts), 1:totpts));
        
        % Compute the Lebesgue function
        lebfun = sum(abs(V(:, 1:totpts)*Vinv), 2);
        
        % Find the Lebesgue constant
        [lebconst, lebconstarg] = max(lebfun);
        
        % Append the location of the newest greedy-add points
        gpts_locs{idx}(totpts+1) = lebconstarg;
        
        % Update the number of points we have
        totpts = totpts+1;
    end
    
    % Populate the vector that stores the locations of the greedy-add
    % points for the next total degree with the current vector of
    % greedy-add point locations
    if td < tds(end)
        gpts_locs{idx+1}(1:totpts) = gpts_locs{idx};
    end
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to all the greedy-add points
    numptsg(idx) = 100*length(find(gpts_locs{idx} > bind))/...
        totpts;
    
    % Find the number of selected greedy-add points that are boundary
    % points compared to the number of total boundary points
    numptsg_b(idx) = 100*length(find(gpts_locs{idx} > bind))/...
        length(xbl);
end

%% Plot the results
% Plot distribution of boundary and interior AFP's on L shape
h_afp = figure;
filename_afp = 'animated_AFP_L.gif';

for idx = 1:length(tds)
    b_afps_locs = afps_locs{idx}(afps_locs{idx} > bind);
    int_afps_locs = afps_locs{idx}(afps_locs{idx} <= bind);
    plot(x(b_afps_locs), y(b_afps_locs), 'b.', ...
        x(int_afps_locs), y(int_afps_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis square;
    title('Distribution of boundary and interior AFPs on L shape');
    xlabel('x');
    ylabel('y');
    drawnow;
    
    % Capture plot as image
    frame = getframe(h_afp);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    
    % Write to the GIF file
    if idx == 1
        imwrite(imind, cm, filename_afp, 'gif', 'LoopCount', inf);
    else
        imwrite(imind, cm, filename_afp, 'gif', 'WriteMode', 'append');
    end
end

% Plot distribution of boundary and interior GA points on L shape
h_ga = figure;
filename_ga = 'animated_GA_L.gif';

for idx = 1:length(tds)
    b_gpts_locs = gpts_locs{idx}(gpts_locs{idx} > bind);
    int_gpts_locs = gpts_locs{idx}(gpts_locs{idx} <= bind);
    plot(x(b_gpts_locs), y(b_gpts_locs), 'b.', ...
        x(int_gpts_locs), y(int_gpts_locs), 'r.', ...
        'MarkerSize', 10);
    xlim([-1, 1]);
    ylim([-1, 1]);
    axis square;
    title('Distribution of boundary and interior GA points on L shape');
    xlabel('x');
    ylabel('y');
    drawnow;
    
    % Capture plot as image
    frame = getframe(h_ga);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    
    % Write to the GIF file
    if idx == 1
        imwrite(imind, cm, filename_ga, 'gif', 'LoopCount', inf);
    else
        imwrite(imind, cm, filename_ga, 'gif', 'WriteMode', 'append');
    end
end

% figure;
% plot(tds, numptsqr, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total selected pts. (AFP)');
% hold on;
% plot(tds, numptsg, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total selected pts. (greedy-add points)');
% %title('L domain (Equispaced node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
% title('L domain (Chebyshev node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total selected pts.');
% xlabel('Total degree');
% ylabel('Percentage of boundary points picked divided by total selected pts.');
% ylim([0, 100]);
% legend(L, 'best');
% 
% figure;
% plot(tds, numptsqr_b, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total boundary pts. (AFP)');
% hold on;
% plot(tds, numptsg_b, LW, 2, DN, ...
%     'Percentage of boundary pts. picked divided by total boundary pts. (greedy-add pts)');
% %title('L domain (Equispaced node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total boundary pts.');
% title('L domain (Chebyshev node boundary discretization) - Total degree vs. percentage of boundary pts. picked divided by total boundary pts.');
% xlabel('Total degree');
% ylabel('Percentage of boundary pts. picked divided by total boundary pts.');
% ylim([0, 10]);
% legend(L, 'best');