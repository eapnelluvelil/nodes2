% Investigate how many boundary points the QR algorithm picks
clc;
clear;

% Specify the number of sides the regular polygon domains
% will have
sides = 3:6;

% Specify the number of interpolation points
num_interp_pts = 150;
find_td = roots([1, 1, -2*num_interp_pts]);
td = ceil(find_td(find_td > 0));

num_cols = td*(td+1)/2;

% Track the number of points used
num_afps = 2:2:num_cols;

% Track the percentage of boundary points that are selected
% from all the AFPs
percent_b_pts_selected = cell(length(sides));

for idx = 1:length(sides)
    percent_b_pts_selected{idx} = zeros(1, length(num_afps));
end

for idx = 1:length(sides)
    N = 300;
    n = sides(idx);
    
    %% Generate discretization of the domain
    [xb, yb, xx, yy, in] = PolygonPointGen(N, n);
    
    % Find the points within the boundary
    x_in_b = xx(in);
    y_in_b = yy(in);
    bound_ind = length(x_in_b);
    
    % Remove domain boundary points
    common_pts = intersect([x_in_b, y_in_b], [xb(:), yb(:)], 'rows');
    in_pts = setxor([x_in_b, y_in_b], common_pts, 'rows');
    
    % Find the index at which we add the domain boundary points
    ind = size(in_pts, 1);
    pts = [in_pts; xb(:), yb(:)];
    
    % Find the number of boundary points
    num_b_pts = length(xb);
    
    %% Create the Vandermonde matrix
    Vqr = zeros(size(pts, 1), num_cols);
    
    colqr = 0;
    for k = 0:td
        for j = 0:td
            if (j+k) < td
                Vqr(:, colqr+1) = cos(k*acos(pts(:, 1))).*cos(j*acos(pts(:, 2)));
                
                for p = 1:colqr
                    comp = Vqr(:, colqr+1)'*Vqr(:, p);
                    Vqr(:, colqr+1) = Vqr(:, colqr+1)-comp*Vqr(:, p);
                end
                
                colnorm = norm(Vqr(:, colqr+1));
                Vqr(:, colqr+1) = Vqr(:, colqr+1)/colnorm;
                
                colqr = colqr+1;
            end
        end
    end
    
    %% Run the column-pivoting QR algorithm
    for i = 2:2:num_cols
        % Select a submatrix of the Vandermonde matrix
        Vafps = Vqr(:, 1:i);
        
        % Find the locations of the AFP's
        rand_b = rand(size(Vafps, 2), 1);
        afps_locs = Vafps'\rand_b;
        afps_locs = find(afps_locs);
        
        % Find the number of selected AFPs that are domain
        % boundary points
        percent_b_pts_selected{idx}(floor(i/2)) = ...
            100.0*length(find(afps_locs > ind))/length(afps_locs);
    end
    
    %% Plot the results
    figure;
    plot(num_afps, percent_b_pts_selected{idx}, ...
        'LineWidth', 2);
    title_str = strcat(num2str(n), ...
        '-sided domain: Number of AFPs vs. percentage of selected boundary points');
    title(title_str);
    xlabel('Number of AFPs');
    ylabel('Percentage of selected boundary points');
end