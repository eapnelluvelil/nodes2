% Investigate the growth of Lebesgue constants with and without
% domain boundary points on the disk domain in the square [-1, 1]^2
% using the two algorithsm
clc;
clear;

% Specify how fine we want our mesh to be
N = 300;

[xx, yy] = meshgrid(linspace(-1, 1, N));

%% Generate disk domain
theta = linspace(0, 2*pi, 3*N);

xbdisk = cos(theta);
ybdisk = sin(theta);
indisk = inpolygon(xx, yy, xbdisk, ybdisk);

% Find the index that separates interior points from boundary points
bind = length(xx(indisk));

x = [xx(indisk); xbdisk(:)];
y = [yy(indisk); ybdisk(:)];

%% Specify total degrees for interpolation
tds = 0:30;

%% Track Lebesgue constants for the two algorithms
lebconsts_afps = cell(length(tds), 1);
lebconsts_afps_b = lebconsts_afps;
lebconsts_g = lebconsts_afps;
lebconsts_g_b = lebconsts_afps;

for idx = 1:length(tds)
    lebconsts_afps{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    lebconsts_afps_b{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    lebconsts_g{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
    lebconsts_g_b{idx} = zeros(nchoosek(tds(idx)+2, 2), 1);
end

%% Generate Vandermonde matrices for both algorithms
V = zeros(length(xx(indisk)), nchoosek(tds(end)+2, 2));

c = 0;
for k = 0:tds(end)
    for j = 0:tds(end)
        if (j+k) <= tds(end)
            V(:, c+1) = cos(k*acos(x)).*cos(j*acos(y));
            
            for p = 1:c
                
            end
        end
    end
end
