% Name: PolygonPointGen
% Inputs:
%   N - Number of rows and cols. of the meshgrid
%   n - Number of sides the polygon will have
% 
% Outputs:
%   xb - x-values of the parametric curve defining the polygon
%   yb - y-values of the parametric curve defining the polygon
%   xx - x-coordinates of the grid containing the polygon
%   yy - y-coordinates of the grid containing the polygon
%   in - Logical matrix specifying which entries in xx and yy are
%           within the polygon defined by xb and yb
% 
% Purpose:
%   PolygonPointGen creates a parametric curve defining an n-sided
%   regular polygon. It then generates an N x N meshgrid and finds which
%   points within the meshgrid lie on and/or within the polygon.

function [xb, yb, xx, yy, in] = PolygonPointGen(N, n)
    % Generate polygon boundary
    theta = linspace(0, 2*pi, (2*N)+1);
    
    xb = cos(theta).*(cos(pi/n)./(cos(2*(pi/n)*((theta/(2*(pi/n)))-...
        floor(theta/(2*(pi/n))))-(pi/n))));
    yb = sin(theta).*(cos(pi/n)./(cos(2*(pi/n)*((theta/(2*(pi/n)))-...
        floor(theta/(2*(pi/n))))-(pi/n))));
    
    % Rescale points to be within [-1, 1] x [-1, 1]
    xscale = max(xb);
    xb = xb/xscale;
    
    yscale = max(yb);
    yb = yb/yscale;
    
    % Generate meshgrid
    lower_lim = -max(max(xb), max(yb));
    upper_lim = -1*lower_lim;
    
    [xx, yy] = meshgrid(linspace(lower_lim, upper_lim, N));
    
    % Find points within the polygon
    in = inpolygon(xx, yy, xb, yb);
end