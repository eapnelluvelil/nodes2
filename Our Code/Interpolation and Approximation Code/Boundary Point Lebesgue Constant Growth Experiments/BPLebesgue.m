% Investigate the growth of Lebesgue constants with and without
% domain boundary points
clc;
clear;

% Number of sides of the regular polygon domains
sides = 3;

% Specify max total degree
max_td = 30;
num_cols = max_td*(max_td+1)/2;

% Size of the meshgrid
N = 300;

% Store the Lebesgue constants
leb_consts_afps = cell(length(sides));
for idx = 1:length(sides)
    leb_consts_afps{idx} = zeros(1, length(1:max_td));
end

leb_consts_afps_b = cell(length(sides));
for idx = 1:length(sides)
    leb_consts_afps_b{idx} = zeros(1, length(1:max_td));
end

for idx = 1:length(sides)
    %% Generate discretization of the domain
    n = sides(idx);
    [xb, yb, xx, yy, in] = PolygonPointGen(N, n);
    
    % Append domain boundary points to vector of points
    % of domain points
    x = [xx(in); xb(:)];
    y = [yy(in); yb(:)];
    
    %% Generate Vandermonde matrix
    Vqr = zeros(length(x), num_cols);
    
    colqr = 0;
    for i1 = 0:max_td
        for i2 = 0:max_td
            if (i1+i2) < max_td
                Vqr(:, colqr+1) = cos(i1*acos(x)).*cos(i2*acos(y));
                
                for p = 1:colqr
                    comp = Vqr(:, p)'*Vqr(:, colqr+1);
                    Vqr(:, colqr+1) = Vqr(:, colqr+1) - comp*Vqr(:, p);
                end
                
                colnorm = norm(Vqr(:, colqr+1));
                Vqr(:, colqr+1) = Vqr(:, colqr+1)/colnorm;
                
                colqr = colqr+1;
            end
        end
    end

    %% Run column-pivoting QR algorithm
    for td = 1:max_td
    % for td = 1:(max_td*(max_td+1)/2)
        col = td*(td+1)/2;
        % col = td;
        
        % Select AFP's
        rand_b = rand(size(Vqr(1:length(xx(in)), 1:col), 2), 1);
        afps_locs = Vqr(1:length(xx(in)), 1:col)'\rand_b;
        afps_locs = find(afps_locs);
        
        % Compute the Lagrange functions using only the AFP's
        lagrange_coeffs_afps = Vqr(afps_locs, 1:col)\...
            eye(length(afps_locs));
        
        lagrange_funcs_afps = Vqr(1:length(xx(in)), 1:col)*...
            lagrange_coeffs_afps;
        
        % Compute the Lebesgue function and associated Lebesgue constant
        leb_afps = sum(abs(lagrange_funcs_afps), 2);
        leb_consts_afps{idx}(td) = max(leb_afps);
        
        % Compute the Lagrange functions using the AFP's and the domain
        % boundary points
        lagrange_pts_locs = [afps_locs; ...
            (length(xx(in))+1:length(xx(in))+length(xb))'];
        
        lagrange_coeffs_afps_b = Vqr(lagrange_pts_locs, 1:col)\...
            eye(length(lagrange_pts_locs));
        
        lagrange_funcs_afps_b = Vqr(:, 1:col)*lagrange_coeffs_afps_b;
        
        % Compute the Lebesgue function and the associate Lebesgue constant
        leb_afps_b = sum(abs(lagrange_funcs_afps_b), 2);
        leb_consts_afps_b{idx}(td) = max(leb_afps_b);
    end
    
    %% Plot the results
    figure;
    semilogy(1:max_td, leb_consts_afps{idx}, ...
        'LineWidth', 2, 'DisplayName', 'without boundary points');
    hold on;
    semilogy(1:max_td, leb_consts_afps_b{idx}, ...
        'LineWidth', 2, 'DisplayName', 'with boundary points');
    title_str = strcat(num2str(n), ...
        '-sided regular polygon: Total degree vs. Lebesgue constant');
    title(title_str);
    xlabel('Total degree');
    ylabel('Lebesgue constant');
    legend;
end