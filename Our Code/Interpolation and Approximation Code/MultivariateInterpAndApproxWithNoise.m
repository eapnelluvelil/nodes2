clear;
clc;

% Seed the RNG so that we can compare results on multiple runs
rng(0);

% Control how much noise we want to introduce
noise = 1e-5;

% Construct interpolants and approximants for f and g
f = @(x, y) cos(3*x) + sin(2*y);
g = @(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2));

% Construct interpolants for these total degrees
% When constructing LS approximants, we only use basis functions of degree up to half of the given total degree
total_degs = 1:40;

% To keep track of max error btw. f and g and their interpolants
max_err_f_interp_noise = 0*total_degs;
max_err_g_interp_noise = 0*total_degs;

% To keep track of max error btw. f and g and their least squares approximants
max_err_f_approx_noise = 0*total_degs;
max_err_g_approx_noise = 0*total_degs;

% To keep track of # of AFP's used to construct interpolants and LS approximants
num_afps_interp = 0*total_degs;
% num_afps_approx = 0*total_degs;

% Define the boundary of region
N = 200;
theta = linspace(0, 2*pi, (2*N)+1)';
xb = (1+0.125*cos(12*theta)).*cos(theta);
yb = (1+0.125*cos(12*theta)).*sin(theta);

% Select points within the boundary
min_val = -max(abs(max(xb)), abs(max(yb)));
max_val = max(abs(max(xb)), abs(max(yb)));
[XX, YY] = meshgrid(linspace(min_val, max_val, N));

% Scale the points to be within [-1, 1] x [-1, 1]
x_scale = max(max(XX));
XX = XX/x_scale;

y_scale = max(max(YY));
YY = YY/y_scale;

in_boundary_inds = inpolygon(XX(:), YY(:), xb, yb);

x_in_boundary = XX(in_boundary_inds);
y_in_boundary = YY(in_boundary_inds);

% Evaluate f at all the candidate points
f_cand = f(x_in_boundary, y_in_boundary);
f_cand = f_cand(:);

% Evaluate g at all the candidate points
g_cand = g(x_in_boundary, y_in_boundary);
g_cand = g_cand(:);

% For testing purposes
% We remove a circle of radius r from within the boundary
% r = 0.4;
% circ_inds = (x_in_boundary).^2 + (y_in_boundary).^2 < r^2;

% x_in_boundary(circ_inds) = [];
% y_in_boundary(circ_inds) = [];

for idx = 1:length(total_degs)
	d = total_degs(idx);
    
    % Generate our Vandermonde matrix
    A = zeros(length(x_in_boundary), d*(d+1)/2);
    
    curr_col = 0;
    for k = 0:d
        for j = 0:d
            % If the total degree of the next basis function is less than the
            % total degree, we generate the next basis function and perform GS
            % on it
            if (j+k) <= d
                a_new = cos(k*acos(x_in_boundary)) .* ...
                    cos(j*acos(y_in_boundary));
                
                % Perform GS on the newest column of A
                for i = 1:curr_col
                    a_curr = A(:, i);
                    a_new = a_new - ...
                        ((a_curr'*a_new)/(a_curr'*a_curr))*a_curr;
                end
                
                % Update A
                a_new = a_new/norm(a_new);
                A(:, curr_col+1) = a_new;
                curr_col = curr_col+1;
            end
        end
    end
    
    % Pick the points corresponding to approximate Fekte points via \
    rand_b = rand(size(A, 2), 1);
    afp_inds = A'\rand_b;
    afp_inds = (afp_inds ~= 0);
    
    % Evaluate f at the AFP's and add noise
    f_afps = f(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    f_afps = f_afps(:);
    f_afps = f_afps + noise*(2*rand(size(f_afps))-1);
    
    % Evaluate g at the AFP's and add noise
    g_afps = g(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    g_afps = g_afps(:);
    g_afps = g_afps + noise*(2*rand(size(g_afps))-1);
    

    % When constructing our interpolants, we solve for the Lagrange functions evaluated at all the candidate
    % points in one step
    % We don't explicitly construct the Lagrange functions and then evaluate them at all the candidiate 
    % points as this would be more expensive

    % Construct our interpolant for f
    coeffs_f_interp = A(afp_inds, :)\f_afps;
    f_interp = A*coeffs_f_interp;
    f_interp = f_interp(:);

    % Construct our interpolant for g
    coeffs_g_interp = A(afp_inds, :)\g_afps;
    g_interp = A*coeffs_g_interp;
    g_interp = g_interp(:);

    % Compute max error btw. f and its interpolant
    max_err_f_interp_noise(idx) = max(abs(f_cand) - abs(f_interp));

    % Compute max error btw. g and its interpolant
    max_err_g_interp_noise(idx) = max(abs(g_cand) - abs(g_interp));

    % Store the # of AFP's used in calucating the interpolants
    num_afps_interp(idx) = size(A, 2);

    % When constructing our least squares approximations, we solve for the half of the Lagrange functions
    % evaluated at all the candidate points in one step (like we did when constructing our interpolants)
    % This is to save on computation time

    % In constructing our LS approximants, we only use half as many basis functions as # of AFP's
    % Hence, when we construct the LS approximants, we use only half as many AFP's as when we constructed
    % the interpolants
    num_basis_funcs = floor(length(find(afp_inds))/2);

    % Construct our LS approximant for f
    coeffs_f_approx = A(afp_inds, 1:num_basis_funcs)\f_afps;
    f_approx = A(:, 1:num_basis_funcs)*coeffs_f_approx;
    f_approx = f_approx(:);

    % Construct our LS approximant for g
    coeffs_g_approx = A(afp_inds, 1:num_basis_funcs)\g_afps;
    g_approx = A(:, 1:num_basis_funcs)*coeffs_g_approx;
    g_approx = g_approx(:);

    % Compute max error btw. f and its LS approximant
    max_err_f_approx_noise(idx) = max(abs(f_cand) - abs(f_approx));

    % Compute max error btw. g and its LS approximant
    max_err_g_approx_noise(idx) = max(abs(g_cand) - abs(g_approx));

    % Store the # of AFP's used in calculating the LS approximants
    % num_afps_approx(idx) = num_basis_funcs;
end

%%
% Load the data from Tony's greedy algorithm (with noise)
load('GreedyDataNoise.mat');

% Plot the # of AFP's used in the construction of the interpolants and LS approximants vs.
% log(error) with noise
semilogy(num_afps_interp, max_err_f_interp_noise, 'DisplayName', 'Max interp. err. for f', ...
	'LineWidth', 2);
hold on;
semilogy(num_afps_interp, max_err_f_approx_noise, 'DisplayName', 'Max approx. err. for f', ...
	'LineWidth', 2);
hold on;
semilogy(num_pts, max_err_f_approx_greedy_noise, 'DisplayName', ...
    'Max approx. err. for f (greedy)', 'LineWidth', 2);
hold on;

semilogy(num_afps_interp, max_err_g_interp_noise, 'DisplayName', 'Max interp. err. for g', ...
	'LineWidth', 2);
hold on;
semilogy(num_afps_interp, max_err_g_approx_noise, 'DisplayName', 'Max approx. err. for g', ...
	'LineWidth', 2);
hold on;
semilogy(num_pts, max_err_g_approx_greedy_noise, 'DisplayName', ...
    'Max approx. err. for g. (greedy)', 'LineWidth', 2);
hold off;

title("Number of points vs. $\log(error)$ (with noise)", 'interpreter', 'latex', 'FontSize', ...
	15);
xlabel("Number of AFP's", 'interpreter', 'latex', 'FontSize', 15);
ylabel("$\log(error)$", 'interpreter', 'latex', 'FontSize', 15)
legend;