% Total degrees for our 2D polynomial interpolant
total_degs = 1:45;

% Store our Lebesgue constants
lconsts = zeros(size(total_degs));

% Store the number of approximate Fekete points
num_afps = zeros(size(total_degs));

% Construct our polynomial interpolant for various total degrees
for idx = 1:length(total_degs)
    d = total_degs(idx);
    
    % Define the boundary of region
    theta = linspace(0, 2*pi, 1000)';
    xb = (1+0.1*cos(12*theta)).*cos(theta);
    yb = (1+0.1*cos(12*theta)).*sin(theta);
    
    % Select points within the boundary
    min_val = -max(max(xb), max(yb));
    max_val = max(max(xb), max(yb));
    [XX, YY] = meshgrid(1.1*linspace(min_val, max_val, 100));
    
    in_boundary_inds = inpolygon(XX(:), YY(:), xb, yb);
    
    x_in_boundary = XX(in_boundary_inds);
    y_in_boundary = YY(in_boundary_inds);
    
    % For testing purposes
    % We remove a circle of radius r from within the boundary
    r = 0.4;
    circ_inds = (x_in_boundary).^2 + (y_in_boundary).^2 < r^2;
    
    x_in_boundary(circ_inds) = [];
    y_in_boundary(circ_inds) = [];
    
    % Generate our Vandermonde matrix
    A = zeros(length(x_in_boundary), d*(d+1)/2);
    
    curr_col = 0;
    for k = 0:d
        for j = 0:d
            % If the total degree of the next basis function is less than the
            % total degree, we generate the next basis function and perform GS
            % on it
            if (j+k) <= d
                a_new = cos(k*acos(x_in_boundary)) .* ...
                    cos(j*acos(y_in_boundary));
                
                % Perform GS on the newest column of A
                for i = 1:curr_col
                    a_curr = A(:, i);
                    a_new = a_new - ...
                        ((a_curr'*a_new)/(a_curr'*a_curr))*a_curr;
                end
                
                % Update A
                a_new = a_new/norm(a_new);
                A(:, curr_col+1) = a_new;
                curr_col = curr_col+1;
            end
        end
    end
    
    % Pick the points corresponding to approximate Fekte points via \
    rand_b = rand(size(A, 2), 1);
    afp_inds = A'\rand_b;
    afp_inds = (afp_inds ~= 0);
    
    % Solve for the Lagrange function coefficients
    coeffs = A(afp_inds, :)\eye(size(A(afp_inds, :)));
    
    % Construct our Lagrange functions
    lagrange = ones(size(A));
    
    for k = 1:size(coeffs, 2)
        lagrange(:, k) = A*coeffs(:, k);
    end
    
    % Construct our Lebesgue function
    lebesgue = sum(abs(lagrange), 2);
    
    % Compute our Lebesgue constant and store it
    lconst = max(lebesgue);
    lconsts(idx) = lconst;
    
    % Store the number of calculated AFP's
    num_afps(idx) = size(A, 2);
end

% Plot the growth of the Lebesgue constants as a function of number of
% approximate Fekete points 
semilogy(num_afps, lconsts);
xlabel("Number of AFP's", 'interpreter', 'latex');
ylabel('$\log(\Lambda)$', 'interpreter', 'latex')