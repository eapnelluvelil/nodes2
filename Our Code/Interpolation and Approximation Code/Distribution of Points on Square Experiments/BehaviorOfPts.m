% Investigate the behavior of the approximate Fekete points (AFPs),
% Padua points, and Tony's greedy-add points on the unit square
% [-1, 1] x [1, 1]
clc;
clear;

% Set flags for plotting
MFC = 'MarkerFaceColor';
FS = 'FontSize';
DN = 'DisplayName';
L = 'Location';

% Set display names
PP = 'Pauda pts.';
AFP = 'AFPs';
GA = 'Greedy-add pts';

% Number of bins to use in either direction
nbins = [20, 20];

% Specify for what degrees we want to generate points
tds = 9;
maxcol = (tds(end)+1)*(tds(end)+2)/2;

% Generate discretization of unit square
N = 1000;
minval = -1;
maxval = 1;
[xx, yy] = meshgrid(linspace(-1, 1, N));
xx = xx(:);
yy = yy(:);

% Store the AFPs and Tony's greedy pts.
afps_locs = cell(length(tds), 1);
gpts_locs = cell(length(tds), 1);

% Store the Lebesgue constants computed while the
% greedy-add alg. is running
lebconsts = cell(length(tds), 1);

% How many random points we should initially sample
numstartpts = 5;

for td = tds
    afps_locs{td-(tds(1)-1)} = zeros((td+1)*(td+2)/2, 1);
    gpts_locs{td-(tds(1)-1)} = zeros((td+1)*(td+2)/2 - numstartpts, 1);
end

%% Generate Vandermonde matrix QR alg. and Tony's greedy-add alg.
V = zeros(length(xx(:)), maxcol);

colqr = 0;
for k = 0:tds(end)
    for j = 0:tds(end)
        if (j+k) <= tds(end)
            V(:, colqr+1) = cos(k*acos(xx(:))).*...
                cos(j*acos(yy(:)));
            
            for p = 1:colqr
                comp = V(:, p)'*V(:, colqr+1);
                V(:, colqr+1) = V(:, colqr+1)-comp*V(:, p);
            end
            
            colnorm = norm(V(:, colqr+1));
            V(:, colqr+1) = V(:, colqr+1)/colnorm;
            
            colqr = colqr+1;
        end
    end
end

%% To kickstart the greedy-add alg.
gpts_locs{1}(1:numstartpts) = randsample(find(ones(size(xx))), ...
    numstartpts);

%% Generate the three sets of pts.
for td = tds
    % For indexing purposes
    idx = td-(tds(1)-1);
    
    %% Generate Padua pts.
    x = paduapts(td);
    
    % Plot Padua points
    figure;
    plot(x(:, 1), x(:, 2), 'ok', MFC, 'k', ...
        DN, PP);
    hold on;
    
    %% Generate AFP's
    numcols = (td+1)*(td+2)/2;
    
    rand_b = rand(numcols, 1);
    afps = V(:, 1:numcols)'\rand_b;
    afps = find(afps);
    
    % Store the locations of the AFPs
    afps_locs{idx} = afps;
    
    % Plot AFPs
    plot(xx(afps), yy(afps), 'ok', MFC, 'r', ...
        DN, AFP);
    hold on;
    
    %% Generate greedy-add pts.
    while length(find(gpts_locs{idx})) < length(gpts_locs{idx})
        % Keep track of the number of rows we currently have filled
        numrows = length(find(gpts_locs{idx}));
        
        % Compute the Lebesgue function given the current set of points
        Vginv = pinv(V(gpts_locs{idx}(1:numrows), 1:numrows));
        
        lebfun = sum(abs(V(:, 1:numrows)*Vginv), 2);
        
        % Find where the Lebesgue function takes its maximum
        [lebconst, lebconst_arg] = max(lebfun);
        lebconsts{idx}(numrows-(numstartpts-1)) = lebconst;
        
        % Update the locations of the optimal points
        gpts_locs{idx}(numrows+1) = lebconst_arg;
    end
    
    % Update the locations of the greedy points for the next iteration
    if td < tds(end)
        gpts_locs{idx+1}(1:length(gpts_locs{idx})) = gpts_locs{idx};
    end
    
    % Plot the greedy-add pts.
    plot(xx(gpts_locs{idx}(1:numstartpts)), ...
        yy(gpts_locs{idx}(1:numstartpts)), 'ok', ...
        MFC, 'g', DN, 'Random pts.');
    hold on;
    plot(xx(gpts_locs{idx}(numstartpts+1:end)), ...
        yy(gpts_locs{idx}(numstartpts+1:end)), 'ok', ...
        MFC, 'b', DN, GA);
    
    
    axis equal;
    axis([-1, 1, -1, 1]);
    title_str = strcat('Dist. of Padua pts., AFPs, and greedy-add pts.', ...
        {' - '}, 'TD = ', {' '}, num2str(td));
    title(title_str);
    xlabel('x', FS, 15);
    ylabel('y', FS, 15);
    legend(L, 'best');
    hold off;
    
    % Generate density map Padua point distribution
    figure;
    hp = histogram2(x(:, 1), x(:, 2), nbins, 'DisplayStyle', 'tile', ...
    'ShowEmptyBins', 'on', 'Normalization', 'pdf');
    title('Density map of Pauda points');
    xlabel('x', FS, 15);
    ylabel('y', FS, 15);
    axis image;
    colorbar;
    c1 = caxis;
    
    % Generate density map for AFP distribution
    figure;
    hafp = histogram2(xx(afps), yy(afps), nbins, 'DisplayStyle', 'tile', ...
    'ShowEmptyBins', 'on', 'Normalization', 'pdf');
    title('Density map of approximate Fekete points');
    xlabel('x', FS, 15);
    ylabel('y', FS, 15);
    axis image;
    colorbar;
    c2 = caxis;
    c3 = [min([c1, c2]), max([c1, c2])];
    caxis(c3);
    
    % Generate density map for greedy-add points
    figure;
    hg = histogram2(xx(gpts_locs{idx}), yy(gpts_locs{idx}), nbins, ...
        'DisplayStyle', 'tile', 'ShowEmptyBins', 'on', ...
        'Normalization', 'pdf');
    title('Density map of greedy-add points');
    xlabel('x', FS, 15);
    ylabel('y', FS, 15);
    axis image;
    colorbar;
    c4 = caxis;
    c5 = [min([c3, c4]), max([c3, c4])];
    caxis(c5);
end