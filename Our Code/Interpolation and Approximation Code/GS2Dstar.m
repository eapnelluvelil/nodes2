clear; clc;
timeto3000=tic;
timeto1000=tic;
timeto4000=tic;
timeto2000=tic;
timeto5000=tic;
timeto6000=tic;
timeto7000=tic;
time3000=[];
time1000=[];
time4000=[];
time2000=[];
time5000=[];
time6000=[];
time7000=[];
start=tic;
tic
[xx, yy, xb, yb, in] = StarNB(525);
meshgentime=toc;
xscale=max(max(xx)); yscale=max(max(yy));
xx=xx(:)/xscale; yy=yy(:)/yscale;
xxs=reshape(xx,size(in));
yys=reshape(yy,size(in));
stopval=15000;
startval=300;
cont = 0;
dnorm=norm(ones(length(xx),1));
AA=zeros(length(xx),stopval/2);
AA(:,1) = ones(length(xx),1)/dnorm;
AAholder=0;
looptime=[];
lebmax=[];
cnumb=[];
numbv=floor(stopval/2);
findn=roots([1,1,-2*numbv]);
n=ceil(findn(findn>0));
r=zeros(1,numbv-1);
for k = 2:n
    for j = 1:k
        cont = cont+1
        if cont>=numbv
            break
        end
        if j==k
            AA(:,cont+1)=AA(:,cont+1-k).*yy;
        else
            AA(:,cont+1)=AA(:,cont+1-(k-1)).*xx;
        end
        %         [Q,R] = qr(AA(in(:),1:cont+1),0);
        %         AA(in(:),1:cont+1)=Q;
        %         AA = AA(:,1:cont+1)/R;
        %         for p=cont+1-(k-1)-(k-2)-(j-1):cont
        for p=1:cont
            r(p)=AA(in(:),p)'*AA(in(:),cont+1);
            AA(:,cont+1)=AA(:,cont+1)-r(p)*AA(:,p);
        end
        dnorm(cont+1)=norm(AA(in(:),cont+1));
        AA(:,cont+1)=AA(:,cont+1)/dnorm(cont+1);
    end
end
locholder=randsample(find(in(:)), startval);
x=xx(locholder);
y=yy(locholder);
A=zeros(stopval,stopval/2);
A(1:length(x),1:length(x)/2)=AA(locholder,1:length(x)/2);
Ainvtime=zeros(1,(stopval-startval)/2);
looptimetime=zeros(1,(stopval-startval)/2);
err=zeros(1,(stopval-startval)/2);
errx=zeros(1,(stopval-startval)/2);
Lebfuncgentime=zeros(1,(stopval-startval)/2);
cnumb=zeros(1,(stopval-startval)/2);
interperr=zeros(1,(stopval-startval)/2);
intererrcalctime=zeros(1,(stopval-startval)/2);
Lebmaxy=zeros(1,(stopval-startval)/2);
plotholder=cont-1;
while length(x)<stopval
    if length(x)==12000
        time3000=toc(timeto3000)
    elseif length(x)==10000
        time1000=toc(timeto1000)
    elseif length(x)==14000
        time4000=toc(timeto4000)
    elseif length(x)==16000
        time2000=toc(timeto2000)
    elseif length(x)==18000
        time5000=toc(timeto5000)
    elseif length(x)==6000
        time6000=toc(timeto6000)
    elseif length(x)==7000
        time7000=toc(timeto7000)
    end
    loop=tic;
    tic
    Ainv = pinv(A(1:length(x),1:length(x)/2));
    Ainvtime(cont-plotholder)=toc;
    Leb = 0*xx;
    tic
    Leb(in(:)) = sum(abs(AA(in(:),1:length(x)/2)*Ainv),2);
    Leb=reshape(Leb,size(in));
    %     Leb = reshape(sum(abs(AA(:,1:length(x)/2)/A),2),size(in)).*in;
    Lebfuncgentime(cont-plotholder)=toc;
    %     err(cont-plotholder)=norm(A*Ainv-eye(length(A*Ainv)),'fro')/sqrt(length(A*Ainv));
    errx(cont-plotholder)=cont;
    cnumb(cont-plotholder)=cond(A(1:length(x),1:length(x)/2));
    tic
    %     interperr(cont-plotholder)=norm(((7*sin(2*xx(in(:)).^3.*yy(in(:)).^5))+(13*cos((9*xx(in(:)).^4).*(17*yy(in(:)).^2))))...
    %         -AA(in(:),1:length(x)/2)*(Ainv*((((7*sin(2*x.^3).*(y.^5))))+(13*cos((9*x.^4).*(17*y.^2))))));
    %     interperr(cont-plotholder)=norm(exp(-(xx(in(:)).^10)./10).*exp(-(yy(in(:)).^3)./37)...
    %         .*((7*sin(2*xx(in(:)).^3.*yy(in(:)).^5))+(13*cos((9*xx(in(:)).^4).*(17*yy(in(:)).^2))))...
    %         -AA(in(:),1:length(x)/2)*(Ainv*(exp(-(x.^10)./10).*exp(-(y.^3)./37)...
    %         .*(((7*sin(2*x.^3).*(y.^5))))+(13*cos((9*x.^4).*(17*y.^2))))));
%     interperr(cont-plotholder)=norm(sin(2*xx(in(:)))+cos(yy(in(:)))-AA(in(:),1:length(x)/2)*(Ainv*(sin(2*x)+cos(y))));
    %         interperr(cont-plotholder)=norm(sin(2*xx(in(:)))+cos(yy(in(:)))-AA(in(:),1:length(x)/2)*(A\(sin(2*x)+cos(y))));
    
            interperr(cont-plotholder)=norm(sin(30*xx(in(:))).*cos(15.*yy(in(:)))-AA(in(:),1:length(x)/2)*(Ainv*(sin(30*x).*cos(15.*y))));
    %         interperr(cont-plotholder)=norm(1./(1+25*(xx(in(:)).^2+yy(in(:)).^2))-...
    %             AA(in(:),1:length(x)/2)*(Ainv*(1./(1+25*(x.^2+y.^2)))));
    
    %             interperr(cont-plotholder)=norm(xx(:).^1-AA(:,[1:length(x)/2])*(Ainv*(x(:).^1)));
    %         interperr(cont-plotholder)=norm(exp(2*xx(:))-AA(:,[1:length(x)/2])*(Ainv*(exp(2*x))));
    intererrcalctime(cont-plotholder)=toc;
    %      disp(['intererrcalctime = '
    %      num2str(intererrcalctime(cont-plotholder))]);s
    %     figure(length(x))
    %     surf(xx,yy,sin(2*xx)+cos(yy))
    %     hold on
    %     surf(xx,yy,reshape(AA*Ainv*(sin(2*x)+cos(y)),size(xx)))
    Lebv=sort(Leb(:));
    maxval=Lebv(length(Lebv)-1:length(Lebv));
    Lebmaxy(cont-plotholder)=maxval(2);
    hLocalMax = vision.LocalMaximaFinder;
    hLocalMax.MaximumNumLocalMaxima = 2;
    hLocalMax.NeighborhoodSize = [101 101];
    hLocalMax.Threshold = 1;
    location = step(hLocalMax, Leb);
    xxloc1=xxs(location(1,2),location(1,1));
    yyloc1=yys(location(1,2),location(1,1));
    xxloc2=xxs(location(2,2),location(2,1));
    yyloc2=yys(location(2,2),location(2,1));
    x=[x;xxloc1;xxloc2];
    y=[y;yyloc1;yyloc2];
    locholder(length(x)-1)=(sub2ind(size(xxs),location(1,2),location(1,1)));
    locholder(length(x))=(sub2ind(size(xxs),location(2,2),location(2,1)));
    A(length(x)-1:length(x),1:length(x)/2-1)=AA(locholder(length(x)-1:length(x)),1:length(x)/2-1);
    A(1:length(x),length(x)/2)=AA(locholder,length(x)/2);
    cont=cont+1
    looptime(cont-plotholder-1)=toc(loop);
    looptime(cont-plotholder-1)
    %     figure(length(x))
    %     surf(xxs,yys,Leb), hold on, shading interp
    %     hold on
    %     plot3(xxloc1,yyloc1,Leb(location(1,2),location(1,1)),'*')
    %     hold on
    %     plot3(xxloc2,yyloc2,Leb(location(2,2),location(2,1)),'*')
        if mod(length(x),1000)==0
      save('LData','Lebmaxy','x', 'y', 'Lebfuncgentime', 'intererrcalctime', 'Ainvtime', 'looptime', 'cnumb', 'interperr')
      end
end
errx=errx-stopval/2+1;
x=x*xscale; y=y*yscale;
% figure
% semilogy(errx,Lebmaxy)
% title('Leb Constant')
% figure
% plot(x(startval+1:end),y(startval+1:end),'*')
% hold on
% plot(x(1:startval),y(1:startval),'.')
% title('nodes')
% figure
% semilogy(errx,Lebfuncgentime)
% hold on
% semilogy(errx,intererrcalctime,'r')
% hold on
% semilogy(errx,Ainvtime,'g')
% hold on
% semilogy(errx,looptime,'b')
% title('Time to calculate')
% legend('Calculate Lebesgue Function','Interpolation Error','Calculate Ainv','Looptime')
% figure
% semilogy(errx,cnumb)
% hold on
% semilogy(errx,interperr)
% title('Condition number of A')
totaltime=toc(start)
% save('LData','Lebmaxy','x', 'y', 'Lebfuncgentime', 'intererrcalctime', 'Ainvtime', 'looptime', 'cnumb', 'interperr','time3000','time1000','time4000','time2000','time5000','time6000','time7000','totaltime')