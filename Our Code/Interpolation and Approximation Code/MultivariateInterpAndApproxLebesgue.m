rng(0);

noise = 0;

% Set the number of AFP's we want to compute
num_afps = 500;

% Given that we want to generate n points, we want to
% pick the max, total degree of the basis functions to
% be such that (d choose 2) = n
find_max_total_deg = roots([1, 1, -2*num_afps]);
max_total_deg = ceil(find_max_total_deg(find_max_total_deg > 0));

% Construct interpolants and approximants for f and g
f = @(x, y) cos(3*x) + sin(2*y);
g = @(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2));

% Define the boundary of region
N = 200;
theta = linspace(0, 2*pi, (2*N)+1)';
xb = (1+0.125*cos(12*theta)).*cos(theta);
yb = (1+0.125*cos(12*theta)).*sin(theta);

% Create discretization of the region
min_val = -max(abs(max(xb)), abs(max(yb)));
max_val = max(abs(max(xb)), abs(max(yb)));
[xx, yy] = meshgrid(linspace(min_val, max_val, N));

% Select points on and within the boundary
in = inpolygon(xx, yy, xb, yb);

% Scale the points to be within [-1, 1] x [-1, 1]
x_scale = max(max(xx));
xx = xx(:)/x_scale;

y_scale = max(max(yy));
yy = yy(:)/y_scale;

% Reshape xx and yy into squares
xxs = reshape(xx, size(in));
yys = reshape(yy, size(in));

x_in_boundary = xx(in);
y_in_boundary = yy(in);

% Evaluate f at all the candidate points
f_cand = f(x_in_boundary, y_in_boundary);
f_cand = f_cand(:);

% Evaluate g at all the candidate points
g_cand = g(x_in_boundary, y_in_boundary);
g_cand = g_cand(:);

% For testing purposes
% We remove a circle of radius r from within the boundary
% r = 0.4;
% circ_inds = (x_in_boundary).^2 + (y_in_boundary).^2 < r^2;
% 
% x_in_boundary(circ_inds) = [];
% y_in_boundary(circ_inds) = [];

%% Column-pivoting QR algorithm
% When constructing LS approximants, we only use basis functions of degree 
% up to half of the given total degree
total_degs = 1:max_total_deg;

% To keep track of max error btw. f and g and their interpolants
max_err_f_interp = 0*total_degs;
max_err_g_interp = 0*total_degs;

% Keep track of max error btw. f and g and their LS approximants
max_err_f_approx = 0*total_degs;
max_err_g_approx = 0*total_degs;

% To keep track of # of AFP's used to construct interpolants and LS approximants
num_afps_interp = 0*total_degs;
num_afps_approx = 0*total_degs;

% To keep track of the Lebesgue constants for interpolation and LS approximation
leb_consts_interp = 0*total_degs;
leb_consts_approx = 0*total_degs;
    
% Generate our Vandermonde matrix
V_afps = zeros(length(x_in_boundary), max_total_deg*(max_total_deg+1)/2);

% Store the components of subsequent columns of the Vandermonde matrix in
% the directions of the previous columns
r = zeros(1, (max_total_deg*(max_total_deg+1)/2)-1);

% Keep track of the current column of the Vandermonde matrix
curr_col = 0;
for k = 0:max_total_deg
    for j = 0:max_total_deg
        if (j+k) <= max_total_deg
            % Generate next column of the Vandermonde matrix
            V_afps(:, curr_col+1) = cos(k*acos(x_in_boundary)).*...
                cos(j*acos(y_in_boundary));
            
            % Make the current column orthgonal to the previous columns
            for p = 1:curr_col
                r(p) = V_afps(:, p)'*V_afps(:, curr_col+1);
                V_afps(:, curr_col+1) = V_afps(:, curr_col+1) - ...
                    r(p)*V_afps(:, p);
            end
            
            % Normalize the current column of the Vandermonde matrix
            curr_col_norm = norm(V_afps(:, curr_col+1));
            V_afps(:, curr_col+1) = V_afps(:, curr_col+1)/curr_col_norm;
            
            % Update the column index
            curr_col = curr_col+1;
        end
    end
end
    
for idx = 1:length(total_degs)
    d = total_degs(idx);
    
    % Generate our Vandermonde matrix given the current total degree
    V_afps_d = V_afps(:, 1:d*(d+1)/2);
    
    % Pick the points corresponding to approximate Fekte points via \
    rand_b = rand(size(V_afps_d, 2), 1);
    afp_inds = V_afps_d'\rand_b;
    afp_inds = (afp_inds ~= 0);
    
    % Evaluate f at the AFP's and add noise
    f_afps = f(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    f_afps = f_afps(:);
    f_afps = f_afps + noise*(2*rand(size(f_afps))-1);
    
    % Evaluate g at the AFP's and add noise
    g_afps = g(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    g_afps = g_afps(:);
    g_afps = g_afps + noise*(2*rand(size(g_afps))-1);

    % When constructing our interpolants, we solve for the Lagrange functions evaluated at all the candidate
    % points in one step
    % We don't explicitly construct the Lagrange functions and then evaluate them at all the candidiate 
    % points as this would be more expensive

    % Uncomment the following code if you don't want to calculate
    % the Lebesgue constant at each iteration

    % Construct our interpolant for f
    coeffs_f_interp = V_afps_d(afp_inds, :)\f_afps;
    f_interp = V_afps_d*coeffs_f_interp;
    f_interp = f_interp(:);

    % Construct our interpolant for g
    coeffs_g_interp = V_afps_d(afp_inds, :)\g_afps;
    g_interp = V_afps_d*coeffs_g_interp;
    g_interp = g_interp(:);

    % Compute the coefficients for the Lagrange functions 
    coeffs_lagrange_funcs_interp = V_afps_d(afp_inds, :)\eye(size(V_afps_d(afp_inds, :)));

    % Compute the Lagrange functions
    lagrange_funcs_interp = ones(size(V_afps_d));

    for i = 1:size(coeffs_lagrange_funcs_interp, 2)
        lagrange_funcs_interp(:, i) = V_afps_d*coeffs_lagrange_funcs_interp(:, i);
    end

    % Construct our interpolant for f
    % f_interp = lagrange_funcs_interp*f_afps;

    % Construct our interpolant for g
    % g_interp = lagrange_funcs_interp*g_afps;

    % Compute max error btw. f and its interpolant
    max_err_f_interp(idx) = max(abs(f_cand) - abs(f_interp));

    % Compute max error btw. g and its interpolant
    max_err_g_interp(idx) = max(abs(g_cand) - abs(g_interp));

    % Store the number of points used in calucating the interpolants
    num_afps_interp(idx) = size(V_afps_d, 2);

    % When constructing our least squares approximations, we solve for the half of the Lagrange functions
    % evaluated at all the candidate points in one step (like we did when constructing our interpolants)
    % This is to save on computation time

    % Uncomment the following code if you don't want to 
    % calculate the Lebesgue constant at each iteration
    
    % In constructing our LS approximants, we only use half as many basis functions as # of AFP's
    % Hence, when we construct the LS approximants, we use only half as many AFP's as when we constructed
    % the interpolants
    num_basis_funcs = floor(length(find(afp_inds))/2);

    % Construct our LS approximant for f
    coeffs_f_approx = V_afps_d(afp_inds, 1:num_basis_funcs)\f_afps;
    f_approx = V_afps_d(:, 1:num_basis_funcs)*coeffs_f_approx;
    f_approx = f_approx(:);

    % Construct our LS approximant for g
    coeffs_g_approx = V_afps_d(afp_inds, 1:num_basis_funcs)\g_afps;
    g_approx = V_afps_d(:, 1:num_basis_funcs)*coeffs_g_approx;
    g_approx = g_approx(:);

    % Compute the coefficients for the Lagrange functions for
    % LS approximations
    coeffs_lagrange_funcs_approx = V_afps_d(afp_inds, 1:num_basis_funcs)\ ...
        eye(size(V_afps_d(afp_inds, :)));

    % Compute our Lagrange functions for the LS approximations
    lagrange_funcs_approx = V_afps_d(:, 1:num_basis_funcs)* ...
        coeffs_lagrange_funcs_approx;

    % Construct our LS approximation for f
    % f_approx = lagrange_funcs_approx*f_afps;

    % Construct our LS approximation for g
    % g_approx = lagrange_funcs_approx*g_afps;

    % Compute max error btw. f and its LS approximant
    max_err_f_approx(idx) = max(abs(f_cand) - abs(f_approx));

    % Compute max error btw. g and its LS approximant
    max_err_g_approx(idx) = max(abs(g_cand) - abs(g_approx));

    % Store the # of points used in calculating the LS approximants
    num_afps_approx(idx) = num_basis_funcs;

    % Calculate the Lebesgue constant when doing interpolation
    leb_fun_interp = sum(abs(lagrange_funcs_interp), 2);
    leb_consts_interp(idx) = max(leb_fun_interp);

    % Calculate the Lebesgue constant when doing LS approximation
    leb_fun_approx = sum(abs(lagrange_funcs_approx), 2);
    leb_consts_approx(idx) = max(leb_fun_approx);
end

%% Tony's algorithm
% Keep track of the max error btw. f and g and their LS approximations
% using Tony's algorithm
max_err_f_approx_greedy = [];
max_err_g_approx_greedy = [];

% Keep track of the number of points used to construct the LS
% approximations using Tony's algorithm
num_greedy_pts = [];

% Number of points we initially sample at random
start_val = 10;

% Number of points we subsequently compute
stop_val = 500;

% Create the Vandermonde matrix
VV_greedy = zeros(length(xx), floor(stop_val/2));
VV_greedy(:, 1) = ones(length(xx), 1)/norm(ones(length(xx), 1));

% Don't really know what this code does but keep it
% just in case we need it
VV_holder = 0;
loop_time = [];
leb_max = [];
cond_numb = [];

% We use half as many basis functions as the number of points
% we will eventually compute
numbv = floor(stop_val/2);

% This code finds the max total degree of the basis functions 
findn = roots([1, 1, -2*numbv]);
n = ceil(findn(findn > 0));
r = zeros(1, numbv-1);

% Generate subsequent columns of the Vandermonde matrix 
% and pre-condition them
% Keep track of which column we're working with


% Randomly sample points within the boundary
loc_holder = randsample(find(in(:)), start_val);
x_greedy = xx(loc_holder);
y_greedy = yy(loc_holder);

% We use a Vandermonde submatrix whose rows correspond to the picked points
% We use this matrix for constructing LS approximations
V_greedy = zeros(stop_val, numbv);
V_greedy(1:length(x_greedy), 1:floor(length(x_greedy/2))) = ...
    VV_greedy(loc_holder, 1:floor(length(x_greedy/2)));

% Keep track of V_greedy's inversion time
V_greedy_inv_time = zeros(1, floor((stop_val-start_val)/2));

% Keep track of the Lebesgue constant
Lebmax_y = [];

while length(x_greedy) < stop_val
    % Compute the pseudo-inverse of the subset of the Vandermonde matrix
    V_greedy_inv = pinv(V_greedy(1:length(x_greedy), ...
        1:floor(length(x_greedy)/2)));

    % Compute our Lebesgue function
    Leb = 0*xx;
    Leb(in(:)) = sum(abs(VV_greedy(in(:), ...
        1:floor(length(x_greedy)/2)))*V_greedy_inv, 2);
    Leb = reshape(Leb, size(in));

    % Evaluate f and g at the greedy alg's points and add noise
    f_greedy_pts = f(x_greedy, y_greedy); f_greedy_pts = f_greedy_pts(:);
    f_greedy_pts = f_greedy_pts + noise*(2*rand(size(f_greedy_pts))-1);

    g_greedy_pts = g(x_greedy, y_greedy); g_greedy_pts = g_greedy_pts(:);
    g_greedy_pts = g_greedy_pts + noise*(2*rand(size(g_greedy_pts))-1);

    % Construct our LS approximation to f and g using the greedy alg's points
    f_greedy_approx = VV_greedy(in(:), 1:floor(length(x_greedy)/2))*...
                        V_greedy_inv*f_greedy_pts;
    g_greedy_approx = VV_greedy(in(:), 1:floor(length(x_greedy)/2))*...
                        V_greedy_inv*g_greedy_pts;

    % Compute the max error btw. f and g and their LS approximations
    max_err_f_approx_greedy = [max_err_f_approx_greedy, ...
                                norm(abs(f_cand)-abs(f_greedy_approx), 'inf')];
    max_err_g_approx_greedy = [max_err_g_approx_greedy, ...
                                norm(abs(g_cand)-abs(g_greedy_approx), 'inf')];

    % Track the number of points used to calculate the LS approximations
    num_greedy_pts = [num_greedy_pts, floor(length(x_greedy)/2)];

    % Compute the Lebesgue constant
    Lebv = sort(Leb(:));
    max_vals = Lebv(length(Lebv)-1:length(Lebv));
    Lebmax_y = [Lebmax_y, max_vals(2)];

    % Find the Lebesgue functions two maxima given our
    % domain discretization
    hLocalMax = vision.LocalMaximaFinder;
    hLocalMax.MaximumNumLocalMaxima = 2;
    % Why is the neighborhood size set to this?
    hLocalMax.NeighborhoodSize = [101, 101];
    hLocalMax.Threshold = 1;
    location = step(hLocalMax, Leb);
    xx_loc_1 = xxs(location(1, 2), location(1, 1));
    yy_loc_1 = yys(location(1, 2), location(1, 1));
    xx_loc_2 = xxs(location(2 ,2), location(2, 1));
    yy_loc_2 = yys(location(2, 2), location(2, 1));

    % Store the locations of the two maxima
    x_greedy = [x_greedy; xx_loc_1; xx_loc_2];
    y_greedy = [y_greedy; yy_loc_1; yy_loc_2];

    % Update the locations of the sampled points
    loc_holder(length(x_greedy)-1) = (sub2ind(size(xxs), ...
        location(1, 2), location(1, 1)));
    loc_holder(length(x_greedy)) = (sub2ind(size(xxs), ...
        location(2, 2), location(2, 1)));

    % Update the Vandermonde submatrix
    V_greedy(length(x_greedy)-1:length(x_greedy), ...
        1:floor(length(x_greedy)/2)-1) = ...
        VV_greedy(loc_holder(length(x_greedy)-1:length(x_greedy)), ...
                    1:floor(length(x_greedy)/2)-1);
    V_greedy(1:length(x_greedy), ...
                floor(length(x_greedy)/2)) = ...
        VV_greedy(loc_holder, floor(length(x_greedy)/2));
end

%%
% Load the data from Tony's greedy algorithm
% load('GreedyData.mat');

% Plot the # of AFP's used in the construction of the interpolants and LS approximants vs.
% log(error)
% semilogy(num_afps_interp, max_err_f_interp, 'DisplayName', 'Max interp. err. for f', ...
% 	'LineWidth', 2);
% hold on;
% semilogy(num_afps_approx, max_err_f_approx, 'DisplayName', 'Max approx. err. for f', ...
% 	'LineWidth', 2);
% hold on;
semilogy(num_greedy_pts, max_err_f_approx_greedy, 'DisplayName', ...
   'Max approx. err. for f (greedy)', ...
   'LineWidth', ...
   2);
% hold on;
% semilogy(num_afps_interp, max_err_g_interp, 'DisplayName', 'Max interp. err. for g', ...
% 	'LineWidth', 2);
% hold on;
% semilogy(num_afps_approx, max_err_g_approx, 'DisplayName', 'Max approx. err. for g', ...
% 	'LineWidth', 2);
hold on;
semilogy(num_greedy_pts, max_err_g_approx_greedy, 'DisplayName', ...
   'Max approx. err. for g (greedy)', ...
   'LineWidth', ...
   2);
hold off;
title("Number of points used vs. $\log(error)$ (with noise)", ...
    'interpreter', 'latex', 'FontSize', 15);
xlabel("Number of points", 'interpreter', 'latex', 'FontSize', 15);
ylabel("$\log(error)$", 'interpreter', 'latex', 'FontSize', 15)
legend;

% Plot the Lebesgue constants for interpolation and LS approximations for
% AFP's
figure;
% semilogy(num_afps_interp, leb_consts_interp, 'LineWidth', 2, 'DisplayName', ...
%     'Leb. const. for interp. (AFP)');
% hold on;
% semilogy(num_afps_approx, leb_consts_approx, 'LineWidth', 2, 'DisplayName', ...
%     'Leb. const. for LS approx. (AFP)');
% hold on;
% Plot the Lebesgue constants for Tony's points
semilogy(num_greedy_pts, Lebmax_y, 'LineWidth', 2, 'DisplayName', ...
    'Leb. const. for LS approx. (Tony)');
hold off;
legend;
title("Number of points used vs. Lebesgue constant", 'interpreter', ...
    'latex', 'FontSize', 15);
xlabel("Number of points", 'interpreter', 'latex', ...
    'FontSize', 15);
ylabel("$\Lambda$", 'interpreter', 'latex', 'FontSize', 15);
legend;