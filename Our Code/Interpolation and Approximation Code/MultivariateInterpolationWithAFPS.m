% We want to construct interpolants for these multivariate functions
f = @(x, y) cos(3*x)+sin(2*y);
g = @(x, y) (1./(1+25*x.^2)) + (1./(1+25*y.^2));

% We construct interpolants of the following total degrees
total_degs = 1:50;

%% For interpolation
% We keep track of the max error between the interpolant and the function
% for each total degree
max_interp_errors_f = 0*total_degs;
max_interp_errors_g = 0*total_degs;

% We keep track of the number of approximate Fekete points
num_afps_interp = 0*total_degs;

%% For least squares approximation
% We keep track of the max error between the functions and the approximants
max_approx_errors_f = 0*total_degs;
max_approx_errors_g = 0*total_degs;

% We keep track of the number of AFP's used in constructing the LS
% approximation
num_afps_approx = 0*total_degs;

% We evaluate the functions and interpolants over a star-shaped region with
% a circle cut out of it
for idx = 1:length(total_degs)
    %% For interpolation and least squares approximation
    d = total_degs(idx);
    
    % Define the boundary of region
    theta = linspace(0, 2*pi, 1000)';
    xb = (1+0.1*cos(12*theta)).*cos(theta);
    yb = (1+0.1*cos(12*theta)).*sin(theta);
    
    % Select points within the boundary
    min_val = -max(max(xb), max(yb));
    max_val = max(max(xb), max(yb));
    [XX, YY] = meshgrid(1.1*linspace(min_val, max_val, 100));
    
    in_boundary_inds = inpolygon(XX(:), YY(:), xb, yb);
    
    x_in_boundary = XX(in_boundary_inds);
    y_in_boundary = YY(in_boundary_inds);
    
    % For testing purposes
    % We remove a circle of radius r from within the boundary
    r = 0.4;
    circ_inds = (x_in_boundary).^2 + (y_in_boundary).^2 < r^2;
    
    x_in_boundary(circ_inds) = [];
    y_in_boundary(circ_inds) = [];
    
    % Generate our Vandermonde matrix
    A = zeros(length(x_in_boundary), d*(d+1)/2);
    
    curr_col = 0;
    for k = 0:d
        for j = 0:d
            % If the total degree of the next basis function is less than the
            % total degree, we generate the next basis function and perform GS
            % on it
            if (j+k) <= d
                a_new = cos(k*acos(x_in_boundary)) .* ...
                    cos(j*acos(y_in_boundary));
                
                % Perform GS on the newest column of A
                for i = 1:curr_col
                    a_curr = A(:, i);
                    a_new = a_new - ...
                        ((a_curr'*a_new)/(a_curr'*a_curr))*a_curr;
                end
                
                % Update A
                a_new = a_new/norm(a_new);
                A(:, curr_col+1) = a_new;
                curr_col = curr_col+1;
            end
        end
    end
    
    % Pick the points corresponding to approximate Fekte points via \
    rand_b = rand(size(A, 2), 1);
    afp_inds = A'\rand_b;
    afp_inds = (afp_inds ~= 0);
    
    % Evaluate the functions at the AFP's
    f_afps = f(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    f_afps = f_afps(:);
    
    g_afps = g(x_in_boundary(afp_inds), y_in_boundary(afp_inds));
    g_afps = g_afps(:);
    
    %% For interpolation
    % Solve for the Lagrange function coefficients for interpolation
    coeffs_interp = A(afp_inds, :)\eye(size(A(afp_inds, :)));
    
    % Construct our Lagrange functions for interpolation
    lagrange_interp = ones(size(A));
    
    for k = 1:size(coeffs_interp, 2)
        lagrange_interp(:, k) = A*coeffs_interp(:, k);
    end
    
    % Construct our interpolant using the calculated Lagrange functions and
    % function values
    f_interpolant = lagrange_interp*f_afps;
    f_interpolant = f_interpolant(:);
    
    g_interpolant = lagrange_interp*g_afps;
    g_interpolant = g_interpolant(:);
    
    % Compute the max error between the constructed interpolants and the
    % functions when evaluated at the candidate points
    f_vals = f(x_in_boundary, y_in_boundary);
    f_vals = f_vals(:);
    
    g_vals = g(x_in_boundary, y_in_boundary);
    g_vals = g_vals(:);
    
    max_interp_error_f = max(abs(f_vals) - abs(f_interpolant));
    max_interp_errors_f(idx) = max_interp_error_f;
    
    max_interp_error_g = max(abs(g_vals) - abs(g_interpolant));
    max_interp_errors_g(idx) = max_interp_error_g;
    
    % Store the number of calculated AFP's for interpolation
    num_afps_interp(idx) = size(A, 2);
    
    %% For least squares approximation
    % Solve for the Lagrange function coefficients via least squares
    % We use half as many columns as AFP's
    col_val = floor(length(find(afp_inds))/2);
    coeffs_approx = A(afp_inds, 1:col_val)\eye(size(A(afp_inds, :)));
    
    % Construct our Lagrange function
    lagrange_approx = A(:, 1:col_val)*coeffs_approx;
    
    % Construct our approximants using the calculated Lagrange functions
    % and function values
    f_approx = lagrange_approx*f_afps;
    f_approx = f_approx(:);
    
    g_approx = lagrange_approx*g_afps;
    g_approx = g_approx(:);
    
    max_approx_error_f = max(abs(f_vals) - abs(f_approx));
    max_approx_errors_f(idx) = max_approx_error_f;
    
    max_approx_error_g = max(abs(g_vals) - abs(g_approx));
    max_approx_errors_g(idx) = max_approx_error_g;
    
    % Store the number of calculated AFP's for approximation
    num_afps_approx(idx) = col_val;
end

%% Plot max interp. and LS approx. error on semilog plot
figure(10);
semilogy(num_afps_interp, max_interp_errors_f, 'DisplayName', ...
    'Max interpolation error between f and interpolant');
hold on;
semilogy(num_afps_interp, max_interp_errors_g, 'DisplayName', ...
    'Max interpolation error between g and interpolant');
hold on;
semilogy(num_afps_approx, max_approx_errors_f, 'DisplayName', ...
    'Max approximation error between f and approximant')
hold on;
semilogy(num_afps_approx, max_approx_errors_g, 'DisplayName', ...
    'Max approximation error between g and approximant')
legend;
title("Number of AFP's vs. $\log(error)$", "interpreter", "latex")
xlabel("Number of AFP's", "interpreter", "latex");
ylabel("$\log(error)$", "interpreter", "latex");