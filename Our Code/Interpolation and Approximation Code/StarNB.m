function [xx,yy,xb,yb,in,Nb]=StarNB(N)
    % function [xx,yy,xb,yb,in,Nb,normal]=PeanutNB(N)
    Nb = 2*N;
    T = linspace(0,2*pi,Nb+1);
    xb = []; 
    yb = [];
    for i=1:Nb
        t=T(i);
        xb(end+1)=(1 + .125*sin(12*t)).*cos(t);
        yb(end+1)=(1 + .125*sin(12*t)).*sin(t);
        %     xb(end+1)=(1 + .9*sin(4*t)).*cos(t);
        %     yb(end+1)=(1 + .9*sin(4*t)).*sin(t);
        %     normal(i,1)=1;
        %     normal(i,2)=2*sin(t)*(3*cos(t)^2-2)/((6*cos(t)^2-7)*cos(t));
        %     normal(i,:)=normal(i,:)/norm(normal(i,:));
        %     if (t>pi/2)&(t<=3*pi/2)
        %         normal(i,:)=-normal(i,:);
        %     end
    end
    x1=linspace(-max(max(abs(xb)),max(abs(yb))),max(max(abs(xb)),max(abs(yb))), N);
    [xx,yy]=meshgrid(x1);
    % x2=xx(:); y2=yy(:);
    %x2=x2+(0.5/N)*(2*rand(size(x2))-1); always commented
    %y2=y2+(0.5/N)*(2*rand(size(y2))-1); always commented
    in=inpolygon(xx,yy,xb,yb);

    % x2=reshape(x2.*in,[N,N]); %No border keeping matrix shape
    % y2=reshape(y2.*in,[N,N]);
    %
    % x=[xx(in); xx(:)];
    % y=[yy(in); yy(:)];
    % xscale=max(abs(xb));
    % yscale=max(abs(yb));
    % x=x/xscale;
    % y=y/yscale;
    % xb=xb/xscale;
    % yb=yb/yscale;
end