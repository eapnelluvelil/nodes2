f = @(x) -7/3+4*x-2/3*x.^2;

x = linspace(0,5, 100);
x1 = [1,2,4];
y = [1,3,3];

f(x)

plot(x1,y,'*')
hold on;
plot(x, f(x),'-r')
xlim([0,5])
ylim([-4,4])