% Number of points to interpolate with
n = 100;

% Create 1000 equally spaced discretization points
xx = linspace(-1, 1, 1000)';

% Keep track of the Lebesgue constants for the equispaced points and
% Chebyshev points
equi_leb_consts = zeros(n, 1);
cheb_leb_consts = zeros(n, 1);

for i = 1:n
    % Generate i+1 equally spaced points on [-1, 1]
    x1 = linspace(-1, 1, i+1);
    % Generate i+1 Chebyshev points on [-1, 1]
    x2 = chebpts(i+1);
    
    % Create the discretized Lebesgue function for the equispaced points
    L1 = ones(length(xx), length(x1));
    % Create the discretized Lebesgue function for the Chebyshev points
    L2 = ones(length(xx), length(x2));
    
    for k = 1:length(x1)
        for j = 1:length(x1)
            if j ~= k
                L1(:, k) = L1(:, k).*(xx-x1(j))./(x1(k)-x1(j));
                L2(:, k) = L2(:, k).*(xx-x2(j))./(x2(k)-x2(j));
            end
        end
    end
    
    leb1 = sum(abs(L1), 2);
    leb2 = sum(abs(L2), 2);
    
    % Find the Lebesgue constants
    equi_leb_consts(i) = max(leb1);
    cheb_leb_consts(i) = max(leb2);
end

% Plot how the Lebesgue constants grow as we increase the interpolation
% points on [-1, 1] for equispaced points and Chebyshev points on a
% semilogy plot
semilogy(1:n, equi_leb_consts, '*', 1:n, cheb_leb_consts, 'o'); 