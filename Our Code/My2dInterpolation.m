%c0+c1*x+c2*y+c3*x^2+c4*xy+c5*y^2, so 6 points are needed
n=2;% degree
x=2*rand((n+2)*(n+1)/2,1)-1;
y=2*rand((n+2)*(n+1)/2,1)-1;
z=2*rand((n+2)*(n+1)/2,1);
%A*coeffs=z
A=zeros((n+2)*(n+1)/2);
c=1;
for i=0:n %exponent
    for j=0:i %all terms of ith power
        A(:,c)=(x.^(i-j)).*(y.^j);
        c=c+1;
    end
end
coeffs=A\z;
spacing=30;
X=linspace(-2,2,spacing);
[xx,yy]=meshgrid(X);
Z=zeros(spacing);
c=1;
for i=0:n %exponent
    for j=0:i %all terms of ith power
        Z=Z+coeffs(c)*(xx.^(i-j)).*(yy.^j);
        c=c+1;
    end
end
surf(xx,yy,Z);
hold on
plot3(x,y,z,'*');
        
        
