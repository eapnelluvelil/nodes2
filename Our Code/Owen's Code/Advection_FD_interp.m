clc;
clear all;
close all;

%% How many interp points?
numpts = 3;
midpt = (numpts + 1) / 2;

%% define beginning/ending t values and compute number of time steps
ti = 0;
tf = 12;
dt = .01;
M = (tf - ti)/dt;

%% define spatial grid
N = 30;

xch = chebpts(N);
alpha = .985;
x = asin(alpha*xch)/asin(alpha);

%x = linspace(-1,1,N)'; 
dx = 2 / N;      



%% define initial condition
%yi=sin(x);
f = @(x) sin(pi*x);
% save initial condition as first column of matrix
y(:,1) = f(x);


%% preallocate stuff
%stsize =1 3;

y = f(x); %only exists to create dy
dy = 0*y; %preallocate dy
%figure
%% calculate function values
for j = 1:M
    %% main points
    
    for k = midpt: N - (midpt - 1)
        jj = k - (midpt - 1):k + (midpt - 1);
        w = fdw_general(x(jj), midpt);
        dy(k) = w*y(jj, j);
        
        y(k,j+1) =  dy(k) * dt + y(k,j);
        
    end


    %% boundary conditions    
    %% left boundary condition
    jj = 1:numpts;
    for k = 1:midpt - 1
        w = fdw_general(x(jj), k);
        dy(k) = w*y(jj, j);
        
        y(k,j+1) =  dy(k) * dt + y(k,j);
        
    end
    
    %% right boundary condition
    jj = N - (numpts - 1):N;
    for k = 1:(midpt - 1)
        w = fdw_general(x(jj), numpts-k+1);
        dy(N-k+1) = w*y(jj, j);
        y(N-k+1,j+1) =  dy(k) * dt + y(k,j);
        
    end
    
    plot(x,y(:,j+1), 'k')

    
    axis([-1 1 -2 2])
    pause(.01)
end

