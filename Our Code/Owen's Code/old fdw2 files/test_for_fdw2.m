x = -1:1;
ind = 2;

x = x(:);
bw = baryWeights(x);
n  = length(x);
u = 0;

w = -2 * (bw/bw(ind))./(x(ind) - x)


%% FOR LOOP
for k = 1:n
    if k ~= ind
       v = (bw(k) ./ bw(ind))./(x(ind) - x(k));
       u = u + v;
    end
end
u
%%
u = u - (1 ./ (x(ind) - x));

w1 = w.*u;
w1(ind) = -sum(w1(x~=x(ind)));
w1 = w1.'
%% MATRICES
a = (bw ./ bw(ind))./(x(ind) - x);
a = sum(a(x~=x(ind)));
a
%% 
a = a - (1 ./ (x(ind) - x));

w2 = w.*a;
w2(ind) = -sum(w2(x~=x(ind)));
w2 = w2.'

