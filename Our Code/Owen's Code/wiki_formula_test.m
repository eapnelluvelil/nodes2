x = -2:2;
i  = 3;
n = length(x);
% bw = baryWeights(x)
%
% for j = 1:length(x)
%     f = x(i) - x';
%     f = f(f~=f(j));
%     g = 0*f;
%
%     for l = 1:length(f)
%         g(l) = prod(f(f~=f(l)));
%     end
%
%     weights(j) = sum(g .* bw(j));
% end

w = zeros(n,1);

for j = 1:n
    for ell = 1:n
        
        z =0;
        
        if ell ~= j
            
            for m = 1:n
                y = 1;
                if m ~= j && m ~= ell
                    for k = 1:n
                        if k ~= j && k ~= ell && k ~= m
                            y = y .* (x(i) - x(k))./(x(j) - x(k))
                        end
                    end
                    1./(x(j) - x(m)) * y
                    z = z + 1./(x(j) - x(m)) .* y;
                end
            end
            
            w(j) = w(j) + 1 / (x(j) - x(ell)) .* z;
        end
    end
    w(j)
end

w'
fdw2_bruteForce(x, i)