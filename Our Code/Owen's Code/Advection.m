clc;
clear all;
close all;

%define beginning/ending t values and compute number of time steps
ti = 0;
tf = 12;
dt = .01;
M = (tf - ti)/dt;

%define spatial grid
N = 100;
x(1)=-pi;
dx = 2*pi;
dx = dx/N;
for i=1:N-1
    x(i+1) = x(i) + dx;
end

%define initial condition
yi=sin(x);

% save initial condition as first column of matrix
y(:,1) = yi;


%compute future columns using update formula (using DE)
figure;

for j = 1:M
    for i = 2:N-1
        y(i,j+1) = 1/2 * (y(i+1,j) - y(i-1,j))* dt/dx + y(i,j);
    end

    %boundary conditions
    y(1, j+1) = (y(2,j) - y(1, j)) * dt/dx + y(1,j);
    y(N, j+1) = 1/2*(y(1,j) - y(N-1, j)) * dt/dx + y(N,j);
    
    plot(x,y(:,j+1), 'k')

   % plot(x,y(:,j+1))
    
    axis([-pi pi -2 2])
    drawnow;
end

