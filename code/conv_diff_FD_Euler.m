%% Converction diffusion equation u_t = c u_x + a u_xx on [-1,1] with 
% periodic BC's

%% Define grid points
N = 1000;
h = 2/N;
x = (-1:h:1).';

%% Initial condition
u0 = @(x) exp(-20*(x-.3).^2);%sin(5*pi*x)+sin(pi*x);
%u0 = @(x) sin(5*pi*x)+sin(pi*x);

u = u0(x);  

%% Define FD oprator:
c = sin(10*pi*x)+1.1; a = .01;
% here we assume u consists of only interior values.
%L = @(t,u) a*[2*(u(2)-u(1)); diff(u,2); 2*(u(end-1)-u(end))]/h^2;
k = 2:N;
L = @(t,u) a*(u(k-1)-2*u(k)+u(k+1))/h^2 + (u(k+1)-u(k-1))/(2*h);


%% Time-step with ode45 and plot solution
t = 0;
tmax = 30;
Nt = 10000;
dt = 0.4*h^2/a;
%dt = 0.5*h;
shg
for j = 1:Nt
    plot(x,u,'*-'), ylim([-2 2])
    drawnow
    for jj = 1:100
    %[tt,uu] = ode45(L, [t, t+dt], u);
    u(k) = u(k)+dt*L(t,u);
    u(N+1) = sin(2*t);
    u(1) = 0;
    %u = [0; ui; 0];
    %u = uu(end,:).';
    t = t+dt;
    end
end
       
       
       
       