clear all;

%% import data

heatmap = importdata('heatmap_data.mat');
interp_pts = heatmap.interp_pts;
stencil_pts = heatmap.stencil_pts;
opt_alphas = heatmap.opt_alphas;

max_interp_pts = max(interp_pts);
max_stencil_pts = max(stencil_pts);

a = zeros(max_stencil_pts, max_interp_pts);
a(stencil_pts, interp_pts) = opt_alphas; 

b = 1 - opt_alphas;

% figure(1)
% pcolor(opt_alphas)




% figure(3)
% pcolor(a)

%clear heatmap

%optimal_alphas(stencil_pts, interp_pts)

%% plot

figure(1)
pcolor(opt_alphas)
%surf(b)
%set(gca,'ZScale','log')



