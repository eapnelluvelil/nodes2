%% Naive Householder QR Factorization implementation
m = 10;
n = 5;

A = rand(m, n);
A_copy = A;
Q = eye(m);

for k = 1:n
    a = A(k:m, k);
    e = zeros(length(k:m), 1);
    e(1) = 1;
    if a(1) ~= 0
        v = sign(a(1))*norm(a)*e - a;
    else
        v = norm(a)*e - a;
    end
    
    v = v/norm(v);
    
    F = eye(m);
    F(1:(k-1), 1:(k-1)) = eye(k-1);
    F(1+m-(m-k+1):m, 1+m-(m-k+1):m) = eye(length(1+m-(m-k+1):m)) - 2*(v*v');
    
    Q = F*Q;
    A = F*A;
end
