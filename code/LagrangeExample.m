% Polynomial interpolation with the Lagrange formula
d = 10;
%x = linspace(-1,1,d+1)';
x = chebpts(d+1);
xx = linspace(-1,1,1000)';
% L = ones(length(xx),length(x));
% for k = 1:length(x)
%     for j = 1:length(x)
%         if j~=k
%             L(:,k) = L(:,k).*(xx-x(j))./(x(k)-x(j));
%         end
%     end
% end
% for k =1:length(x)
%     plot(xx,L(:,k),x(k),1,'*',x,0,'o')
%     ylim([-2 2])
%     pause
% end

w = baryWeights(x);

f = @(x) 1./(1+25*x.^2);

g = bary(xx,f(x),x,w);

plot(g)