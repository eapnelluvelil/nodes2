% approximation degree
d = 30;

% define boundary
theta = linspace(0,2*pi,1000);
xb = (1+0.1*cos(12*theta)).*cos(theta);
yb = (1+0.1*cos(12*theta)).*sin(theta);

% select points inside the boundary
[xx,yy] = meshgrid(1.1*linspace(-1,1,100));
%ind = inpolygon(xx(:),yy(:),xb,yb);
%x = xx(ind);
%y = yy(ind);
x = xx(:);
y = yy(:);
%incirc = x.^2+y.^2 < .2^2;
%x(incirc) = [];
%y(incirc) = [];

% Plot candidate points
figure(10)
plot(xb,yb,'k',.2*cos(theta), .2*sin(theta),'k', x,y,'.')
axis image


A = [];
for k =0:d
    for j = 0:d
        if j+k <= d
            A = [A cos(k*acos(x)).*cos(j*acos(y))];
            %A = [A x.^k.*y.^j];
        end
    end
end
cond(A)
[Q,R] = qr(A,0);


sol = Q'\rand(size(A,2),1);
ind = sol~=0;

% Plot candidate points
figure(20)
plot(xb,yb,'k',.2*cos(theta), .2*sin(theta),'k', x(ind),y(ind),'.')
axis image






