% Polynomial interpolation example - monomial basis
f = @(x) 1./(1+25*x.^2);
d = 20; % polynomial
%x = linspace(-1,1,d+1)';  % interpolation points
x = chebpts(d+1);
%x = 2*rand(d+1,1)-1;
xx = linspace(-1,1,100)'; % evaluation points

A = zeros(length(x));           % interpolation matrix
AA = zeros(length(xx),d+1);     % evaluation matrix
for k = 0:d
    A(:,k+1) = x.^k;
    AA(:,k+1) = xx.^k;
end

coefs = rand(d+1,1);
b = A*coefs;
b2 = b.*(1+0.001*(2*rand(d+1,1)-1));
coefs2 = A\b2;
disp([coefs coefs2]) 

disp('LHS')
norm(coefs-coefs2)/norm(coefs)
disp('RHS')
cond(A)*norm(b-b2)/norm(b)


