function [bestAlpha]= optimal_alpha(numpts,interp_pts)
tic
%% STENCIL SIZE
midpt = (numpts + 1) / 2;

%% initialize x vectors- x values are solved in the for loop 

%CHEB pts 
xch = chebpts(interp_pts);
% KTE pts
    %Number of different alphas to eval KTE pts 
N = 1000; 
    %Create row vector with alphas to iterate through
alpha = linspace(.00001,.9999999,N)';


%% preallocate stuff

%preallocate x & dy


leb = zeros(1, N);
for k = 1:N
    %% KTE pts
     x = asin(alpha(k)*xch)/asin(alpha(k));
     leb(k) = rbflebfunsimp2(x,numpts);
     %error(k) = second_deriv_error_fun(x,f,d2f,numpts(stencil_pts));


end
%i = find(error == min(error));
[lebmin, index] = min(leb);
bestAlpha = alpha(index);
toc