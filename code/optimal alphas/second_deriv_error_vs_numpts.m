clear all; close all;

%This code calculates the error vs. number of points in the stencil.
%Compares 

%% FUNCTIONS

% original function
f = @(x) sin(1000*x); 
%ANALYTIC 2nd deriv 
d2f = @(x) -1000000*sin(1000*x); 


%% generating x values

% # of interp points
interp_pts = 100;

%EQUALLY SPACED points
%x = linspace(-1,1,n).';
    
%CHEB pts (keep uncommented for KTE)
x = chebpts(interp_pts);

% KTE pts

alpha = .985;
x = asin((alpha)*x)/asin(alpha);

%% preallocate stuff

%preallocate dy
y = f(x); 
dy = zeros([1,length(y)]); 

%%
maxpts = 49;
error_x = 3:2:maxpts;
error_lin = zeros(1, maxpts);

for numpts = 3:2:maxpts
    
    error(numpts) = second_deriv_error_fun(x,f,d2f,numpts)
    
    
    midpt = (numpts + 1) / 2;
    
    %% inner points
    for k = midpt: interp_pts - (midpt - 1)
        jj = k - (midpt - 1):k + (midpt - 1);
        %w = fdweights(midpt, x(jj), 2);
        w = fdw2_general(x(jj), midpt);
        dy(k) = w*y(jj);
    end
    
    %% left boundary condition
    jj = 1:numpts;
    for k = 1:midpt - 1
        %w = fdweights(k, x(jj), 2);
        w = fdw2_general( x(jj),k);
        dy(k) = w*y(jj);
    end
    
    
    
    %% right boundary condition
    jj = interp_pts - (numpts - 1):interp_pts;
    for k = 1:(midpt - 1)
        %w = fdweights(numpts-k+1, x(jj), 2);
        w = fdw2_general(x(jj), numpts-k+1);
        dy(interp_pts-k+1) = w*y(jj);
    end
    error_lin(numpts) = max(abs(d2f(x)-dy'))
end

%% plots
figure(1)
plot(x,dy,'*', x, d2f(x), 'o')

figure(2)
semilogy(x,abs(d2f(x)-dy'),'*')
shg

figure(3)
semilogy(error_x, error_lin(error_x))