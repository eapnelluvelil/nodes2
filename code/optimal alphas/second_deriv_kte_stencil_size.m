clear all; close all;

%% FUNCTIONS

% original function
f = @(x) sin(10*x);
%ANALYTIC 2nd deriv
d2f = @(x) -100*sin(10*x);

%% STENCIL SIZE
max_pts = 25;
numpts = 3:2:max_pts;
midpt = (numpts + 1) / 2;

%% initialize x vectors- x values are solved in the for loop

% # of interp points
interp_pts = 25;

%CHEB pts
xch = chebpts(interp_pts);

% KTE pts

%Number of different alphas to eval KTE pts
N = 100;
%Create row vector with alphas to iterate through
alpha = linspace(.00001,.9999999,N)';


%% preallocate stuff



x = zeros(N, interp_pts)';
y = f(x);
dy = zeros([N,interp_pts]);
error_fun = @(x) abs(d2f(x)-dy'); %function for errors
error = zeros([N,1]);
xch = chebpts(interp_pts);

%preallocate storage vectors
minalpha = zeros(1, length(numpts)); 
minerror = minalpha;

for stencil_pts = 1:length(numpts) %loops through different stencil sizes
    numpts(stencil_pts)
    
    for k = 1:N %loops through different alpha values
        k;
        %% KTE pts
        x = asin(alpha(k)*xch)/asin(alpha(k));
        error(k) = second_deriv_error_fun(x,f,d2f,numpts(stencil_pts));
        leb(k) = rbflebfunsimp2(x,numpts(stencil_pts));
        
    end
    [lebmin, index] = min(leb);
    i = find(error == min(error));
    minalpha(stencil_pts) = alpha(index);
    minerror(stencil_pts) = min(error)
    minerrorE(stencil_pts) = alpha(i)

    
end
%% plots
shg
figure(1)
plot(numpts, minalpha, 'o')
title('Stencil size vs. optimal alpha value, LEBESGUE')
xlabel('Stencil size')
ylabel('Optimal alpha value')
ylim([0 1])


figure (2)
plot(numpts, minerror, 'o')
title('Stencil size vs. error at optimal alpha value')
xlabel('Stencil size')
ylabel('Error @ optimal alpha value')

figure (3)
semilogy(numpts, minerrorE, 'o')
title('Stencil size vs. optimal alpha value, ERROR')
xlabel('Stencil size')
ylabel('Optimal alpha value')