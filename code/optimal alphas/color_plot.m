clear all;

%% import data

heatmap = importdata('heatmap_data.mat');
interp_pts = heatmap.interp_pts;
stencil_pts = heatmap.stencil_pts;
opt_alphas = heatmap.opt_alphas;

max_interp_pts = max(interp_pts);
max_stencil_pts = max(stencil_pts);

a = zeros(max_stencil_pts, max_interp_pts);
a(stencil_pts, interp_pts) = opt_alphas; 

b = 1 - opt_alphas;

%clear heatmap

%optimal_alphas(stencil_pts, interp_pts)

%% plot

