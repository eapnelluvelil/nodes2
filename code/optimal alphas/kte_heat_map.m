clear all; close all;

%% STENCIL SIZE 
max_interp_pts = 100;                max_stencil_pts = 25;
interp_pts = 5:1:max_interp_pts;    stencil_pts = 3:2:max_stencil_pts;


%% Create row vector with alphas to iterate through

storage = zeros(length(stencil_pts), length(interp_pts));

for ii = 1:length(interp_pts)
    
    interp_pts(ii)
    numpts = 3:2:interp_pts(ii); %create vector of stencil sizes

    if length(numpts) > length(stencil_pts)
        numpts = numpts(1:length(stencil_pts));
    end
    
    %% preallocate vectors
    xch = chebpts(interp_pts(ii));
    minalpha = zeros(1, length(numpts));
    

    %% loop through stencil sizes
    for jj = 1:length(numpts) 
        numpts(jj);
        %% loop through alpha values
        minalpha(jj) = goldenSearch(0,1,xch,numpts(jj));

    end
    storage(:, ii) = [minalpha, zeros(1, length(stencil_pts) - length(minalpha))]';

    
end

%% plots

storage

