function tmin = goldenSearch(a, b, xch, stencil_size)
%goldenSearch implements the Golden Section Search algorithm to find the
%optimal alpha (the alpha that minimizes the Lebesgue constant)
%   Detailed explanation goes here


kte = @(alpha) asin(alpha*xch)/asin(alpha);

r = (3-sqrt(5))/2;
c = a + r*(b-a);        
d = a + (1-r)*(b-a);    

fc = rbflebfunsimp2(kte(c), stencil_size);
fd = rbflebfunsimp2(kte(d), stencil_size);



while(d-c) > sqrt(eps)*max(abs(c),abs(d))
    if fc >= fd
        z = c + (1-r)*(b-c);
        %[a c d b] <--- [c d z b]
        a = c;
        c = d; fc = fd;
        d = z; fd = rbflebfunsimp2(kte(d), stencil_size);
    else
        z = a + r*(d-a);
        %[a c d b] <--- [a z c d]
        b = d;
        d = c; fd = fc;
        c = z; fc = rbflebfunsimp2(kte(c), stencil_size);
    end
end
tmin = (c+d)/2;