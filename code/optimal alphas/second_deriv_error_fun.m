function err = second_deriv_error_fun(x,f,d2f,numpts, n)
%% How many interp points?
n = length(x);

midpt = (numpts + 1) / 2;

%% preallocate stuff
y = f(x); %exists to create dy
dy = zeros([1,length(y)]); %preallocate dy

%% inner points
for k = midpt: n - (midpt - 1)
    jj = k - (midpt - 1):k + (midpt - 1);
    %w = fdw2_bruteForce(x(jj), midpt);
    w = fdw2_general(x(jj), midpt);
    dy(k) = w*y(jj);
    %[lfunction, lcinner] = lebesgue(x);
    %lcinner
end

%% left boundary condition
jj = 1:numpts;
for k = 1:midpt - 1
    %w = fdw2_bruteForce(x(jj),k);
    w = fdw2_general(x(jj),k);
    dy(k) = w*y(jj);
    %[lfunction, lcleft] = lebesgue(x);
    %lcleft
end

%% right boundary condition
jj = n - (numpts - 1):n;
for k = 1:(midpt - 1)
    %w = fdw2_bruteForce(x(jj), numpts-k+1);
    w = fdw2_general(x(jj), numpts-k+1);
    dy(n-k+1) = w*y(jj);
    %[lfunction, lcright] = lebesgue(x);
    %lcright
end

err = max(abs(dy(2:end-1)'-d2f(x(2:end-1))))/max(abs(dy(2:end-1)));