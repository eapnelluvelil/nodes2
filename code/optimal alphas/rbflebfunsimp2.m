function [lebc, Leb]= rbflebfunsimp2(x,k)
xx=linspace(-1,1,10001)';
% AA=cos(acos(xx)*[0:(k-1)]);
% A=cos(acos(x)*[0:(k-1)]);
ptstree = KDTreeSearcher(x);
idx = knnsearch(ptstree,xx,'K',k);
idx=sort(idx,2);
%create new column for L
L=zeros(length(xx),length(x));
parts=unique(idx,'rows');
for i=1:size(parts,1)
    bb=max(x(parts(i,:)));
    aa=min(x(parts(i,:)));
    xxidx=ismember(idx,parts(i,:),'rows');
    sxx=(2*xx(xxidx)-bb-aa)/(bb-aa);
    sx=(2*x(parts(i,:))-bb-aa)/(bb-aa);
    A = cos(acos(sx)*[0:k-1]);
    AA = cos(acos(sxx)*[0:k-1]);
    ze=sort(setdiff(1:length(x),parts(i,:)));
    zef=[parts(i,:) ze];
    [tmp, svec]=sort(zef);
    Ltmp=AA/A;
    L(xxidx,:)=[Ltmp zeros(size(Ltmp,1),length(ze))];
    L(xxidx,:)=L(xxidx,svec);
end
Leb=sum(abs(L),2);
lebc=max(Leb);