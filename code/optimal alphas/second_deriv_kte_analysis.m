clear all; close all; 

%% FUNCTIONS

% original function
f = @(x) sin(20*x); 
%ANALYTIC 2nd deriv 
d2f = @(x) -400*sin(20*x); 

%% STENCIL SIZE
numpts = 3;
midpt = (numpts + 1) / 2;

%% initialize x vectors- x values are solved in the for loop 

% # of interp points
interp_pts = 100;

%CHEB pts 

xch = chebpts(interp_pts);

% KTE pts

    %Number of different alphas to eval KTE pts 
N= 500;
    %Create row vector with alphas to iterate through
alpha = linspace(.00001,.9999999,N)';

kte = @(alpha) asin(alpha*xch)/asin(alpha);

g = @(x) rbflebfunsimp2(kte(x), 3);
%% preallocate stuff

%preallocate x & dy

x = zeros(N, interp_pts)';
dy = zeros([N,interp_pts]); 
error_fun = @(x) abs(d2f(x)-dy'); %function for errors
error = zeros([N,1]);



% for k = 1:N
%     k
%     
%     y = f(x);
% 
%     %% KTE pts
%      x = asin(alpha(k)*xch)/asin(alpha(k));
%      %error(k) = second_deriv_error_fun(x,f,d2f,numpts);
%      leb(k) = rbflebfunsimp2(x,numpts);
% 
% end
% [lebmin, index] = min(leb);
% leb, alpha(index)
% 
% % figure (1)
% % semilogy(alpha, error, '*')
% % title(strcat(['Alpha vs. error, STENCIL SIZE: ', num2str(numpts)]));
% % xlabel('Alpha')
% % ylabel('Error')
% % xlim([0 .1])
% 
% figure (2)
% plot(alpha, leb, 'o')
% title(strcat(['Alpha vs. lebesgue constant, STENCIL SIZE: ', num2str(numpts)]))
% xlabel('Alpha')
% ylabel('Lebesgue constant')
% xlim([0 1])
% 
% i = find(error == min(error));
% minalpha = alpha(i);
% minerror = min(error);

%goldenSearch(0, 1, xch, numpts)
fminbnd(g, 0, 1)