clear all; close all; 

%% How many interp points?
numpts = 5;
midpt = (numpts + 1) / 2;
%% generate x array
n = 50;
%x = linspace(-1,1,n).';
% f = @(x) sin(2*x);
f = @(x) sin(x);
d2f = @(x) -1*sin(x);


%% KTE pts
xch = chebpts(n);

alpha = .9;
x = asin(alpha*xch)/asin(alpha);

y = f(x); %only exists to create dy
%N = 11; %this controls spacing of alpha
dy = zeros([1,length(y)]); %preallocate dy

%% preallocate stuff



%x = xch;

%% inner points
for k = midpt: n - (midpt - 1)
    jj = k - (midpt - 1):k + (midpt - 1);
    %w = fdweights(midpt, x(jj), 2);
    w = fdw2_bruteForce(x(jj), midpt);
    dy(k) = w*y(jj);
    [lfunction, lcinner] = lebesgue(x);
    lcinner
end

%% left boundary condition
jj = 1:numpts;
for k = 1:midpt - 1
    %w = fdweights(k, x(jj), 2);
    w = fdw2_bruteForce( x(jj),k);
    dy(k) = w*y(jj);
    [lfunction, lcleft] = lebesgue(x);
    lcleft
end



%% right boundary condition
jj = n - (numpts - 1):n;
for k = 1:(midpt - 1)
   %w = fdweights(numpts-k+1, x(jj), 2);
    w = fdw2_bruteForce(x(jj), numpts-k+1);
    dy(n-k+1) = w*y(jj);
    [lfunction, lcright] = lebesgue(x);
    lcright
end


%% plots
figure(1)


plot(x,dy,'*', x, d2f(x), 'o')

figure(2)
semilogy(x,abs(d2f(x)-dy'),'*')
shg