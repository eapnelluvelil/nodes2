% Polynomial interpolation example - monomial basis
%f = @(x) 1./(1+25*x.^2);
d = 10; % polynomial
%x = linspace(-1,1,d+1)';  % interpolation points
x = chebpts(d+1);
%x = 2*rand(d+1,1)-1;
xx = linspace(-1,1,1000)'; % evaluation points

A = zeros(length(x));           % interpolation matrix
AA = zeros(length(xx),d+1);     % evaluation matrix
for k = 0:d
    A(:,k+1) = cos(k*acos(x));
    AA(:,k+1) = cos(k*acos(xx));
end

y = 0*x; y(5) = 1;
coefs = A\eye(length(x)); % polynomial coefficients
pp = AA*coefs;  % evaluation of polynomial

% plot polynomial and the function 
plot(xx,sum(abs(pp),2),'b'), hold on
plot(lebesgue(x),'--r'), hold off

