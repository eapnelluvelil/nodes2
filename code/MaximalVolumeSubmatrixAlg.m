%% Possible implementation of maximal volume submatrix greedy algorithm
m = 1000;
n = 20;

x = linspace(-1, 1, m);
A = gallery('chebvand', n, x);
A_copy = A;

inds = zeros(1, n);

for k = 1:n
    % Compute the 2-norms of each column of A
    norms = zeros(1, n);
    for j = 1:n
        norms(j) = norm(A(:, j));
    end
    
    % Find the longest column of A
    [~, max_arg] = max(norms);
    inds(k) = max_arg;
    
    % Remove the orthogonal projection of the largest norm column from
    % every column in A
    max_norm_col = A(:, max_arg);
    for j = 1:n
        % Compute (a'*b)/(a'*a)
        comp = (max_norm_col'*A(:, j))/(max_norm_col'*max_norm_col);
        A(:, j) = A(:, j) - comp*max_norm_col;
    end
end

% Find the maximum volume submatrix of A
max_vol_submtx = A_copy(:, inds);

%%
clc;
fprintf("Condition number of maximum volume submatrix: %f\n", cond(max_vol_submtx));
fprintf("Determinant of maximum volume submatrix: %f\n", det(max_vol_submtx));