%% ADVECTION

clear all; close all; clc;

%% Stencil size
numpts = 31;

%% Define grid points
N = 100;

alpha = .8236;

xch = chebpts(N+1);
x = asin(alpha*xch)/asin(alpha);

%% Initial condition & initialize matrices
u0 = @(x) exp(-20*(x-.3).^2);
uold = u0(x);  
u = uold;
unew = 0*x;
uxx = zeros(N-1,1);

%% Calculate differentiation matrix
fdw = diff_matrix(numpts, N+1, x);




%% Time-step and plot solution
tmax = 30;
Nt = 10000;
%dt = 1.5*min(diff(x));
dt = 1.5/1000 
t = dt;

k = 1:N;
% e = eig(fdw(k,k));
% plot(dt * e, 'o')
shg

for j = 1:Nt
    plot(x,u,'-'), ylim([-2 2])
    drawnow
    
    for jjj = 1:10 % this is here so we only plot the wave equation every 100 iterations
        
       % Leap-frog step
       K1 = dt * (fdw(k,k) *  u(k));
       K2 = dt * (fdw(k,k) * (u(k) + 1/2*K1));
       K3 = dt * (fdw(k,k) * (u(k) + 1/2*K2));
       K4 = dt * (fdw(k,k) * (u(k) + K3));
       
       
        unew(k) = u(k) + 1/6*(K1 + 2*K2 + 2*K3 + K4);
       
        %unew(k) = 2*u(k)-uold(k)+dt^2*fdw(k,k)*u(k);
        
        % Update boundary conditions
        unew(N+1) = 0;
        
        
        % advance variables in time
        uold = u;
        u = unew;
        t = t+dt;
    end    
end