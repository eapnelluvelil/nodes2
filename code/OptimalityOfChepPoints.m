% Polynomial interpolation with the Lagrange formula
d = 10;
%x = linspace(-1,1,d+1)';
x = chebpts(d+1,2);
xx = linspace(-1,1,1000)';
ell = 1;
for j = 1:length(x)
    ell =ell.*(xx-x(j));
end
plot(xx,ell,'b',[-1 1],2^(-d)*[1 1],'*--k',xx,2^-(d)*cos((d+1)*acos(xx)),'--r')
shg
