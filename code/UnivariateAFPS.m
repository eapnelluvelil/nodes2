tic
m = 100000;
n = 1000;
x = linspace(-1, 1, m)';

% Generate Vandermonde matrix
V = gallery('chebvand', n, x);
b = rand(n, 1);
y = V\b;


% Find indices of "most significant" columns in a greedy manner
inds = find(y);

% Plot Fekete points
plot(x(inds), 0*x(inds), '*');

% Plot Chebyshev points
hold on;
plot(chebpts(n), 0*chebpts(n), 'o');
toc