f = @(x) 1./(1+25*x.^2);
xx = linspace(-1,1,1000);
N = 10:10:200;
error = 0*N;
for k = 1:length(N)
    fch = chebfun(f,N(k));
    error(k) = norm(f(xx)-fch(xx),inf)/norm(f(xx),inf);
end

% figure(1)
% plot(log10(N),log10(error),'*-')
% xlabel('$\log_{10}(N)$','interpreter','latex')
% ylabel('$\log_{10}$(error)','interpreter','latex')
% set(gca,'fontsize',18)
% ylim([-16 0])

figure(2)
semilogy(N,error,'*-',N,exp(-.2*N),'--')
xlabel('$N$','interpreter','latex')
ylabel('error (log scale)','interpreter','latex')
set(gca,'fontsize',18)
ylim([1e-16 1])


