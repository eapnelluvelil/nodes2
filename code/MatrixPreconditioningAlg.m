% % Maximum degree for the basis functions
% % For example, max_deg = 10 implies total degree up to 9 i.e. x^9, x^6*y^3,
% % etc.
% max_deg = 20;
% for d = 2:max_deg
%     A = zeros(d*(d+1)/2, d*(d+1)/2); % We need the dth triangular number of columns
%     xhat = 2*rand(d*(d+1)/2, 1); % Generate two random vectors
%     yhat = 2*rand(d*(d+1)/2, 1);
%     
%     % Normalize the first column vector
%     q1 = ones(d*(d+1)/2, 1);
%     q1 = q1/norm(q1);
%     A(:, 1) = q1;
%     
%     count = 0;
%     for k = 2:d
%         for j = 1:k
%             count = count+1;
%             % Generate subsequent column vectors by multiplying by xhat or
%             % yhat
%             if j == k
%                 qnew = A(:, count+1-k).*yhat;
%             else
%                 qnew = A(:, count+1-(k-1)).*xhat;
%             end
%             % Perform GS orthogonalization on the generated column vectors
%             for p = 1:count
%                 rp = A(:, p)'*qnew;
%                 qnew = qnew - rp*A(:, p);
%             end
%             qnew = qnew/norm(qnew);
%             A(:, count+1) = qnew;
%         end
%     end
% end

%% Possible implementation of the matrix pre-conditioning algorithm
d = 20;
A = zeros(d*(d+1)/2, d*(d+1)/2);
Ahat = zeros(d*(d+1)/2, d*(d+1)/2);
R = zeros(d*(d+1)/2, d*(d+1)/2);
xhat = 2*rand(d*(d+1)/2, 1) - 1;
yhat = 2*rand(d*(d+1)/2, 1) - 1;

q1 = ones(d*(d+1)/2, 1);
q1 = q1/norm(q1);

A(:, 1) = ones(d*(d+1)/2, 1);
Ahat(:, 1) = q1;
R(1, 1) = q1'*ones(d*(d+1)/2, 1);

count = 0;
for k = 2:d
    for j = 1:k
        count = count + 1;
        
        if j == k
            qnew = Ahat(:, count+1-k).*yhat;
        else
            qnew = Ahat(:, count+1-(k-1)).*xhat;
        end
        
        qnew_original = qnew;
        
        for p = 1:count
            rp = Ahat(:, p)'*qnew;
            qnew = qnew - rp*Ahat(:, p);
            % Update entries of R matrix
            R(p, count+1) = Ahat(:, p)'*qnew_original;
        end
        
        qnew = qnew/norm(qnew);
        
        A(:, count+1) = qnew_original;
        Ahat(:, count+1) = qnew;
        R(count+1, count+1) = qnew'*qnew_original;
    end
end