function w = fdw2_general(x,i)% f = fdw2_general(x,ind)
% Finite difference weigths (general nodes);
% Input:
%   x: stencil nodes
%   ind: index where derivative is evaluated,i.e., f'(x(ind)).
% Output
%   w: finite difference weights, so that  f'(x(ind)) = wf(x) 
%
% Rodrigo Platte, Arizona State University, Jan 2018

x = x';
n = length(x);

w = zeros(n,1);
% y = w;
% a= zeros(n,n);
% z = w;
for j = 1:n
    for ell = 1:n
        if ell ~= j
                  
            y = (x(i) - x)./(x(j) - x);
            z = 1./(x(j)-x(x~=x(j)& x~=x(ell)));
            
            a = y(x~=x(j)& x~=x(ell))'*ones(1,n-2);
            a(1:length(a)+1:end) = 1;            
            w(j) = w(j) + 1 / (x(j) - x(ell)) .* (prod(a)*z');
        end
    end
    
end

w = w';
