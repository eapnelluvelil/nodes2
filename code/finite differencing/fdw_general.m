function w = fdw_general(x,ind)
% f = fdw_general(x,ind)
% Finite difference weigths (general nodes);
% Input:
%   x: stencil nodes
%   ind: index where derivative is evaluated,i.e., f'(x(ind)).
% Output
%   w: finite difference weights, so that  f'(x(ind)) = wf(x) 
%
% Rodrigo Platte, Arizona State University, Jan 2018

x = x(:);
bw = baryWeights(x);
w = (bw/bw(ind))./(x(ind)-x);
w(ind) = -sum(w(x~=x(ind)));
w = w.';
