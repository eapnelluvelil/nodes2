n = 41;
%x = linspace(-1,1,n).';
%rng(0)
%x = sort(2*rand(n,1)-1);
xch = chebpts(n);
x = xch;
alpha = 1;
x = asin(alpha*xch)/asin(alpha);

f = @(x) sin(4*x);
df = @(x) 4*cos(4*x);

stsize = 9;
y = f(x);
dy = 0*y;
% for k = 5:n-4
%     jj = k-4:k+4;
%     w = fdw_general(x(jj), 5);
%     dy(k) = w*y(jj);
% end
% jj = 1:9;
% for k = 1:4
%     w = fdw_general(x(jj), k);
%     dy(k) = w*y(jj);
% end
% jj = n-8:n;
% for k = 1:4
%     w = fdw_general(x(jj), 9-k+1);
%     dy(n-k+1) = w*y(jj);
% end
%w = fdw_general(x(n-2:n), 3);
%dy(n) = w*y(n-2:n);

fdw = diff_matrix(stsize, n, x);
dy = fdw * y;


% figure(1)
% plot(x,df(x),'o',x,dy,'*')
% shg
figure(1)
semilogy(x,abs(df(x)-dy),'*')
shg