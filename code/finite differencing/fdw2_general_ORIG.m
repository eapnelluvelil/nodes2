function w = fdw2_general_ORIG(x,i)
% f = fdw_general(x,ind)
% Finite difference weigths (general nodes);
% Input:
%   x: stencil nodes
%   ind: index where derivative is evaluated,i.e., f'(x(ind)).
% Output
%   w: finite difference weights, so that  f'(x(ind)) = wf(x) 
%
% Rodrigo Platte, Arizona State University, Jan 2018


n = length(x);

w = zeros(n,1);

for j = 1:n
    for ell = 1:n
        
        z =0;
        
        if ell ~= j
            
            for m = 1:n
                y = 1;
                if m ~= j && m ~= ell
                    for k = 1:n
                        if k ~= j && k ~= ell && k ~= m
                            y = y .* (x(i) - x(k))./(x(j) - x(k));
                        end
                    end
                    z = z + 1./(x(j) - x(m)) .* y;
                end
            end
            
            w(j) = w(j) + 1 / (x(j) - x(ell)) .* z;
        end
    end
    
end

w = w';
