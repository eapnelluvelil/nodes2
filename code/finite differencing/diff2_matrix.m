function [diff] = diff2_matrix(numpts, N, x)
%diff2_matrix Creates a differentiation matrix given stencil size (numpts)
%and # of interpolation points (N)

%% Generate FD weights (N point stencil):

%calculates the middle of the stencil
midpt = (numpts + 1) / 2;

%% middle points
fdw2 = zeros(N);
for ell = midpt: N - (midpt-1)
    fdw2(ell,ell-(midpt-1):ell+(midpt-1)) = fdw2_general(x((ell-(midpt-1)):(ell+(midpt-1))),midpt);
end

%% left boundary condition
jj = 1:numpts;
for ell = 1:midpt - 1
    fdw2(ell, jj) = fdw2_general(x(jj),ell);
end

%% right boundary condition
jj = N - (numpts - 1):N;
for ell = 1:(midpt - 1)
    fdw2(jj(numpts-ell+1), jj) = fdw2_general(x(jj), numpts-ell+1);
end

diff = fdw2;

end

