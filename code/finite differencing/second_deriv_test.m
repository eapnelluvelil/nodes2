clear all; close all; 

%% How many interp points?
numpts = 5;
midpt = (numpts + 1) / 2;
%% generate x array
n = 100;
xch = chebpts(n);
alpha = 0.905;
x = asin((alpha)*xch)/asin(alpha);

%x = linspace(-1,1,n).';
% f = @(x) sin(2*x);


f = @(x) sin(x);
d2f = @(x) -1*sin(x);


%% KTE pts

y = f(x); %only exists to create dy
%N = 11; %this controls spacing of alpha
dy = zeros([1,length(y)]); %preallocate dy
dy_Brute = dy;

%% preallocate stuff

%x = xch;

% %% inner points
% for k = midpt: n - (midpt - 1)
%     jj = k - (midpt - 1):k + (midpt - 1);
%     w = fdw2_general(x(jj), midpt);
%     dy(k) = w*y(jj);
%     
%     %w_Brute = fdw2_bruteForce(x(jj), midpt);
%     %dy_Brute(k) = w_Brute*y(jj);
% end
% 
% %% left boundary condition
% jj = 1:numpts;
% for k = 1:midpt - 1
%     w = fdw2_general(x(jj),k);
%     dy(k) = w*y(jj);
%     
%     %w_Brute = fdw2_bruteForce(x(jj), k);
%     %dy_Brute(k) = w_Brute*y(jj);
% end
% 
% 
% 
% %% right boundary condition
% jj = n - (numpts - 1):n;
% for k = 1:(midpt - 1)
%     w = fdw2_general(x(jj), numpts-k+1);
%     dy(n-k+1) = w*y(jj);
%     
%     %w_Brute = fdw2_bruteForce(x(jj), numpts-k+1);
%     %dy_Brute(n-k+1) = w_Brute*y(jj);
% end
% 

%% diff matrix

w = diff2_matrix(numpts, n, x)

dy = w*y;



%% plots
figure(1)
plot(x,dy,'*', x, d2f(x), 'o')

figure(2)
title = 'Wiki Formula';
semilogy(x, abs(d2f(x)-dy),'*')
%./abs(d2f(x))
% figure(2)
% title = 'Brute Force Formula';
% semilogy(x,abs(d2f(x)-dy_Brute')./abs(d2f(x)), 'o')

shg