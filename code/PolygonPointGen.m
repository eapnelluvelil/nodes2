function [xx, yy, xb, yb, in, Nb] = PolygonPointGen(N)
    Nb = 2*N;
    
    % Create discretization for parameter
    T = linspace(0, 2*pi, Nb+1);
    
    % Pre-allocate for speed
    xb = zeros(1, Nb);
    yb = zeros(1, Nb);
    
    for i = 1:Nb
        % Calculate the points along the parametric curve
        % In this case, we generate points along a deformed, star-shaped
        % curve
        xb(i) = (1+(1/50)*sin(T(i))).*cos(T(i));
        yb(i) = (1+(1/70)*sin(T(i))).*sin(T(i));
    end
    
    % We find the left and right endpoints for our discrete interval
    min_val = -max(max(abs(xb)), max(abs(yb)));
    max_val = max(max(abs(xb)), max(abs(yb)));
    
    % We find which points are within and on the deformed, star-shaped
    % parametric curve
    x1 = linspace(min_val, max_val, N);
    [xx, yy] = meshgrid(x1);
    in = inpolygon(xx, yy, xb, yb);
end