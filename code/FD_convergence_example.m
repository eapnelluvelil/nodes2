% spacing between data points, h (or Delta x).
H = 10.^(-5:0.5:0);

% Allocate memory 
err1 = 0*H; 
err2 = 0*H; 
err3 = 0*H;
err4 = 0*H;

% Test function
f = @(x) exp(x);
df = @(x) exp(x);

% Approximte derivatives and compute error
for k =1:length(H)
    h = H(k);
    df1 = (f(h) - f(0))/h;          % 1st order
    df2 = (f(h) - f(-h))/(2*h);     % 2nd order
    %x = -h:h:h;
    %w = fdw_general(x,2);
    %df2 = w*f(x(:));
    
    df3 = (-(5/60)*f(2*h)+2/3*f(h)-2/3*f(-h)+5/60*f(-2*h))/h; %4th order
    x = -2*h:h:2*h;
    w = fdw_general(x,3)
    df4 = w*f(x(:))
    
    err1(k) = abs(1-df1);
    err2(k) = abs(1-df2);
    err3(k) = abs(1-df3);
    err4(k) = abs(1-df4+eps);
end

% Plot error
loglog(H,err1,'*-g',H,err2,'*-b',H,err3,'*-r',H,err4,'*-k')

xlabel('$h$','interpreter','latex')
ylabel('error($h$)','interpreter','latex')
hold on

% plot error estimates
loglog(H,H.^1/2,'--g',H,eps./H,'--g',H,H.^2/6,'--b',H,H.^4,'--r')

hold off
%ylim([1e-16 1])
set(gca,'fontsize',16)
grid on
shg    

    