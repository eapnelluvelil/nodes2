x = chebpts(10);
f = chebfun(@(x) exp(x),10);
L = lebesgue(x);
hold on
for k = 1:1000
    y = exp(x)+1e-5*(2*rand(10,1)-1);
    p = chebfun(y);
    plot(abs(p-f),'-r',1e-5*L,'k')
end
hold off
shg