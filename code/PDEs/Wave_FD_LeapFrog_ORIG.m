%% Solves the wave aquation with zeros boundary conditions

%% Define grid points
N = 1000;
h = 2/N;
x = (-1:h:1).';

%% Initial condition
u0 = @(x) exp(-20*(x).^2);
%sin(5*pi*x)+sin(pi*x);
%u0 = @(x) sin(5*pi*x)+sin(pi*x);

uold = u0(x);  
u = uold;
unew = 0*x;

%% Genereate FD weights (3 point stencil):
fdw = zeros(N-1,3);
for ell =2:N
    fdw(ell,:) = fdw_general(x(ell-1:ell+1),2);
end
uxx = zeros(N-1,1);


%% Time-step with ode45 and plot solution
tmax = 30;
Nt = 10000;
dt = .9*h;
t = dt;

%dt = 0.5*h;
shg
k = 2:N;
for j = 1:Nt
    plot(x,u,'-'), ylim([-2 2])
    drawnow
    %pause(0.2)
    for jj = 1:7
        
        for ell =2:N
            uxx(ell-1) =  (1/h^2)*[1 -2 1]*u(ell-1:ell+1);
        end
        
        % Leap-frog step
        unew(k) = 2*u(k)-uold(k)+dt^2*uxx;
        
        % Update boundary conditions
        unew(N+1) = 0;
        unew(1) = 0;
        
        % advance variables in time
        uold = u;
        u = unew;
        t = t+dt;
    end
    
end



