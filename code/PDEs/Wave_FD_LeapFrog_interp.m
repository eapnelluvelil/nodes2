% clear all; close all; clc;

%% Stencil size
numpts = 31;

%% Define grid points
N = 40;

alpha = .8236;

xch = chebpts(N+1);
x = asin(alpha*xch)/asin(alpha);

%% Initial condition & initialize matrices
u0 = @(x) exp(-20*(x).^2);
uold = u0(x);  
u = uold;
unew = 0*x;
uxx = zeros(N-1,1);

%% Calculate differentiation matrix
fdw2 = diff2_matrix(numpts, N+1, x);

%% Time-step and plot solution
tmax = 30;
Nt = 10000;
dt = 1.5*min(diff(x));
t = dt;

ee = eig(fdw2(2:end-1,2:end-1));


min(dt^2*ee), max(imag(ee))

shg
k = 2:N;
for j = 1:Nt
    plot(x,u,'-'), ylim([-2 2])
    drawnow
    
    for jjj = 1:10 % this is here so we only plot the wave equation every 100 iterations
        
       % Leap-frog step
        unew(k) = 2*u(k)-uold(k)+dt^2*fdw2(k,k)*u(k);
        
        % Update boundary conditions
        unew(N+1) = 0;
        unew(1) = 0;
        
        % advance variables in time
        uold = u;
        u = unew;
        t = t+dt;
    end    
end