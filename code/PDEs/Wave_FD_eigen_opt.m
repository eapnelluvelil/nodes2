clear all;

%% Stencil size
numpts = 15;

%% Define grid points
N = 100;
N = N+1;
numalphas = 1000;
alpha = linspace(.00000001, 1, numalphas);

eigenvals = zeros(1, numalphas);


%% Initial condition
for k = 1:numalphas
    k
    xch = chebpts(N);
    x = asin(alpha(k)*xch)/asin(alpha(k));
    
    %% Genereate FD weights (N point stencil):
    
    fdw2 = diff2_matrix(numpts, N, x);
    eigenvals(k) = max(imag(eig(fdw2(2:end-1,2:end-1))));

end

bestalpha = alpha(length(eigenvals(eigenvals == 0)))