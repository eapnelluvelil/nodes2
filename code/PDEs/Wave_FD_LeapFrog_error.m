clc;

%% Stencil size
numpts = 15;
midpt = (numpts + 1) / 2;

%% Define grid points
N = 100;

alpha = .992;
xch = chebpts(N+1);
x = asin(alpha*xch)/asin(alpha);

%% Initial condition & initialize matrices
u0 = @(x) exp(-20*(x).^2);
uold = u0(x);  
u = uold;
unew = 0*x;
uxx = zeros(N-1,1);

%% Calculate differentiation matrix
fdw2 = diff2_matrix(numpts, N+1, x);

%% Construct Time Variables
tmax = 30;
Nt = 50000;
%dt = 1.5*min(diff(x));
t = dt;

%% Calculate Eigenvalues 
ee = eig(fdw2(2:end-1,2:end-1));
min(dt^2*ee), max(imag(ee))

shg
k = 2:N;
ii = 1;
error = zeros(N+1, 15);
for j = 1:Nt
        
        t
        if mod(t,4) < dt
            error(:, ii) = u - u0(x);
            ii = ii+1;
        end
        
       % Leap-frog step
        unew(k) = 2*u(k)-uold(k)+dt^2*fdw2(k,k)*u(k);
        
        % Update boundary conditions
        unew(N+1) = 0;
        unew(1) = 0;
        
        % advance variables in time
        uold = u;
        u = unew;
        t = t+dt;
end

semilogy(x(k), abs(error(k, 1)), 'o')

